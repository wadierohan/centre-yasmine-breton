<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rapport extends Model
{
    protected $fillable = [
        'text', 'date', 'user_id', 'enfant_id'
    ];

    public function enfant()
    {
        return $this->belongsTo('App\Enfant');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeAtDate($query, $year, $month)
    {
        return $query->whereYear('date', $year)->whereMonth('date', $month);
    }
}
