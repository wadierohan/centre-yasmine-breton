<?php

namespace App;

use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    const ROLE_ADMIN = 'admin';
    const ROLE_ASSISTANT = 'assistant';
    const ROLE_MUTUAL = 'mutual';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'service',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'role', 'service_id', 'phone', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Check if the user is admin
     * 
     * @return Boolean
     */
    public function isAdmin() {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * Check if the user is assistant
     * 
     * @return Boolean
     */
    public function isAssistant() {
        return $this->role === self::ROLE_ASSISTANT;
    }

    /**
     * Check if the user is mutual
     * 
     * @return Boolean
     */
    public function isMutual() {
        return $this->role === self::ROLE_MUTUAL;
    }

    /**
     * Scope a query to only include admin users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmin($query) {
        return $query->where('role', self::ROLE_ADMIN);
    }

    /**
     * Scope a query to only include assistant users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAssistant($query) {
        return $query->where('role', self::ROLE_ASSISTANT);
    }

    /**
     * Scope a query to only include mutual users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMutual($query) {
        return $query->where('role', self::ROLE_MUTUAL);
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function enfants()
    {
        return $this->belongsToMany('App\Enfant');
    }

    public function rapports()
    {
        return $this->hasMany('App\Rapport');
    }

    public function rdvs()
    {
        return $this->hasMany('App\Rdv');
    }

    static function getAssistantsArray()
    {
        $users = self::assistant()->get();
        $userArray = [];
        foreach($users as $user) {
            $userArray[$user->id] = $user->name . ' (' . $user->service->name . ')';
        }
        return $userArray;
    }
}
