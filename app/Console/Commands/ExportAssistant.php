<?php

namespace App\Console\Commands;

use App\User;
use App\Assistant;
use Illuminate\Console\Command;

class ExportAssistant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:assistantstousers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all assistants to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $assistants = Assistant::all();
        foreach($assistants as $assistant) {
            if (!$assistant->email) {
                $name = explode(' ', strtolower($assistant->name));
                $email = implode('.', $name);
                $assistant->email = $email.'@centreyasmine.com';
            }
            $user = new User();
            $user->id = $assistant->id;
            $user->name = $assistant->name;
            $user->phone = $assistant->phone;
            $user->email = $assistant->email;
            $user->role = User::ROLE_ASSISTANT;
            $user->password = \bcrypt('123456');
            $user->save();
        }
    }
}
