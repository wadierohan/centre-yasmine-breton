<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Enfant
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $gender
 * @property string $birthdate
 * @property string|null $remarque
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $diagnostic_id
 * @property int $docteur_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Assistant[] $assistants
 * @property-read int|null $assistants_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Assurance[] $assurances
 * @property-read int|null $assurances_count
 * @property-read \App\Diagnostic $diagnostic
 * @property-read \App\Docteur $docteur
 * @property-read mixed $fullname
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Prise[] $prises
 * @property-read int|null $prises_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $services
 * @property-read int|null $services_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tuteur[] $tuteurs
 * @property-read int|null $tuteurs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereDiagnosticId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereDocteurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereRemarque($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Enfant whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Enfant extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'gender', 'birthdate', 'remarque', 'diagnostic_id', 'docteur_id', 'anamnese',
    ];

    protected $appends = [
        'fullname',
    ];

    public function getFullnameAttribute($value)
    {
        return ucfirst($this->firstname) . ' ' . strtoupper($this->lastname);
    }

    public function assurances()
    {
        return $this->belongsToMany('App\Assurance');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function diagnostic()
    {
        return $this->belongsTo('App\Diagnostic');
    }

    public function docteur()
    {
        return $this->belongsTo('App\Docteur');
    }

    public function tuteurs()
    {
        return $this->belongsToMany('App\Tuteur');
    }

    public function prises()
    {
        return $this->hasMany('App\Prise');
    }

    public function rapports()
    {
        return $this->hasMany('App\Rapport');
    }

    public function rdvs()
    {
        return $this->hasMany('App\Rdv');
    }

    public function delete()
    {
        $this->assurances()->detach();
        $this->users()->detach();
        $this->tuteurs()->detach();
        $this->prises()->delete();
        $this->rapports()->delete();
        $this->rdvs()->delete();
        parent::delete();
    }
}
