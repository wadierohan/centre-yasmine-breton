<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Assurance
 *
 * @property int $id
 * @property string $name
 * @property string $fullname
 * @property string $shortname
 * @property string|null $desc
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Enfant[] $enfants
 * @property-read int|null $enfants_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Prise[] $prises
 * @property-read int|null $prises_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereShortname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assurance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Assurance extends Model
{
    protected $fillable = [
        'name', 'fullname', 'shortname', 'desc',
    ];

    public function enfants()
    {
        return $this->belongsToMany('App\Enfant');
    }

    public function prises()
    {
        return $this->hasMany('App\Prise');
    }
}
