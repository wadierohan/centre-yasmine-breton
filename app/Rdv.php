<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rdv extends Model
{
    protected $fillable = [
        'user_id', 'enfant_id', 'dateDebut', 'dateFin',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function enfant()
    {
        return $this->belongsTo('App\Enfant');
    }

    public function delete()
    {
        parent::delete();
    }

    public function scopeFutur($query)
    {
        return $query->where('dateDebut', '>=', Carbon::now()->format('Y-m-d H:i:s'));
    }
}
