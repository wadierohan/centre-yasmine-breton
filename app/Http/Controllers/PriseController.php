<?php

namespace App\Http\Controllers;

use PDF;
use App\Prise;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $prises = Prise::orderBy('created_at', 'desc')->get();
        return view('prise.index', ['prises' => $prises]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('prise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $messages = [
            'date.unique' => "Cet enfant a déjà une prise en charge à cette date",
        ];

        $rules = [
            'code' => 'required',
            'charge' => 'numeric',
            'date' => ['required', 'date', Rule::unique('prises')->where(function ($query) use ($request) {
                return $query
                    ->whereDate('date', $request->date)
                    ->where('enfant_id', $request->enfant_id);
            })],
            'enfant_id' => 'required',
            'facture' => 'required',
            'facture_year' => 'required|numeric',
            'facture_date' => 'date',
        ];
        $request->validate($rules, $messages);
        $prise = new Prise($request->all());
        $prise->assurance_id = 1;
        $prise->save();
        return redirect()->route('prises.index')->with('success', __('Prise en charge ajoutée'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prise  $prise
     *
     * @return \Illuminate\View\View
     */
    public function show(Prise $prise)
    {
        return view('prise.show', ['prise' => $prise]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prise  $prise
     *
     * @return \Illuminate\View\View
     */
    public function edit(Prise $prise)
    {
        return view('prise.edit', ['prise' => $prise]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prise  $prise
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Prise $prise)
    {
        $messages = [
            'date.unique' => "Cet enfant a déjà une prise en charge à cette date",
        ];

        $rules = [
            'code' => 'required',
            'charge' => 'numeric',
            'date' => ['required', 'date', Rule::unique('prises')->ignore($prise->id, 'id')->where(function ($query) use ($request) {
                return $query
                    ->whereDate('date', $request->date)
                    ->where('enfant_id', $request->enfant_id);
            })],
            'enfant_id' => 'required',
            'facture' => 'required',
            'facture_year' => 'numeric',
            'facture_date' => 'date',
        ];
        $request->validate($rules, $messages);
        $prise->update($request->all());
        return redirect(route('prises.index') . '#' . $prise->id)->with('success', __('Prise en charge mise à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prise  $prise
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Prise $prise)
    {
        $prise->delete();
        return redirect()->route('prises.index')->with('success', __('Prise en charge supprimée'));
    }

    public function generatePdf(Prise $prise)
    {
        $title = 'Prise_' . Carbon::parse($prise->date)->format('Y-m') . '_' . $prise->enfant->fullname;
        $prises = [$prise];
        return view('prisepdf', compact('prises', 'title'));
    }
}
