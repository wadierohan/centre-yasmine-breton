<?php

namespace App\Http\Controllers;

use App\User;
use App\Enfant;
use App\Rapport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RapportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $rapports = new Rapport;
        } else {
            $rapports = auth()->user()->rapports();
        }
        $perPage = request()->has('perPage') ? request('perPage') : 10;
        $queries = ['perPage' => $perPage];
        $enfants = auth()->user()->isAdmin()
            ? Enfant::all(['firstname', 'lastname', 'id'])->pluck('fullname', 'id')->toArray()
            : auth()->user()->enfants()->get(['firstname', 'lastname', 'enfants.id'])->pluck('fullname', 'id')->toArray();
        $filterByUser = ['' => 'Filtrer par assistant'] + User::getAssistantsArray();
        $filterByEnfant = ['' => 'Filtrer par enfant'] + $enfants;
        $orderBy = [
            '' => 'Trier par',
            'date' => 'Date',
        ];

        // Filter By user
        if (request()->has('user') && request('user')) {
            // $enfants = $enfants->where($column, request('user'));
            $user = request('user');
            $rapports = $rapports->whereHas('user', function ($query) use ($user) {
                $query->where('user_id', $user);
            });
            $queries['user'] = request('user');
        }

        // Filter By Enfant
        if (request()->has('enfant') && request('enfant')) {
            // $enfants = $enfants->where($column, request('service'));
            $enfant = request('enfant');
            $rapports = $rapports->whereHas('enfant', function ($query) use ($enfant) {
                $query->where('enfant_id', $enfant);
            });
            $queries['enfant'] = request('enfant');
        }

        // Order By
        if (request()->has('orderBy') && request()->has('sort') && request('orderBy') && request('sort')) {
            $rapports = $rapports->orderBy(request('orderBy'), request('sort'));
            $queries['orderBy'] = request('orderBy');
            $queries['sort'] = request('sort');
        } else {
            $rapports = $rapports->orderBy('date', 'desc');
        }

        $rapports = $rapports->paginate($perPage)->appends($queries);

        return view('rapport.index', compact('rapports', 'filterByUser', 'filterByEnfant', 'orderBy', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $users = User::getAssistantsArray();
        if (auth()->user()->isAdmin()) {
            $enfants = Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        } else {
            $enfants = auth()->user()->enfants()->get(['enfants.id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        }
        return view('rapport.create', compact('users', 'enfants'));
    }

    /**
     * Show the form for duplicating a resource.
     *
     * @return \Illuminate\View\View
     */
    public function duplicate(Rapport $rapport)
    {
        $users = User::getAssistantsArray();
        if (auth()->user()->isAdmin()) {
            $enfants = Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        } else {
            $enfants = auth()->user()->enfants()->get(['enfants.id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        }
        return view('rapport.duplicate', compact('users', 'enfants', 'rapport'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if (auth()->user()->isAdmin()) {
            $messages = [
                'date.unique' => "Cet enfant a déjà un rapport à cette date pour le même assistant",
            ];
            $rules = [
                'text' => 'required',
                'enfant_id' => 'required',
                'user_id' => 'required',
                'date' => ['required', 'date', Rule::unique('rapports')->where(function ($query) use ($request) {
                    return $query
                        ->whereDate('date', $request->date)
                        ->where('enfant_id', $request->enfant_id)
                        ->where('user_id', $request->user_id);
                })],
            ];
            $request->validate($rules, $messages);
            $data = $request->all();
        } else {
            $messages = [
                'date.unique' => "Cet enfant a déjà un rapport à cette date",
            ];
            $rules = [
                'text' => 'required',
                'enfant_id' => 'required',
                'date' => ['required', 'date', Rule::unique('rapports')->where(function ($query) use ($request) {
                    return $query
                        ->whereDate('date', $request->date)
                        ->where('enfant_id', $request->enfant_id)
                        ->where('user_id', auth()->id());
                })],
            ];
            $request->validate($rules, $messages);
            $data = $request->all();
            $data['user_id'] = auth()->id();
        }
        $rapport = new Rapport($data);
        $rapport->save();
        return redirect()->route('rapports.index')->with('success', __('Rapport ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rapport  $rapport
     *
     * @return \Illuminate\View\View
     */
    public function show(Rapport $rapport)
    {
        $users = User::getAssistantsArray();
        if (auth()->user()->isAdmin()) {
            $enfants = Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        } else {
            $enfants = auth()->user()->enfants()->get(['enfants.id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        }
        return view('rapport.show', compact('rapport', 'users', 'enfants'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rapport  $rapport
     *
     * @return \Illuminate\View\View
     */
    public function edit(Rapport $rapport)
    {
        $users = User::getAssistantsArray();
        if (auth()->user()->isAdmin()) {
            $enfants = Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        } else {
            $enfants = auth()->user()->enfants()->get(['enfants.id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        }
        return view('rapport.edit', compact('rapport', 'users', 'enfants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rapport  $rapport
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Rapport $rapport)
    {
        if (auth()->user()->isAdmin()) {
            $messages = [
                'date.unique' => "Cet enfant a déjà un rapport à cette date pour le même assistant",
            ];
            $rules = [
                'text' => 'required',
                'enfant_id' => 'required',
                'user_id' => 'required',
                'date' => ['required', 'date', Rule::unique('rapports')->ignore($rapport->id, 'id')->where(function ($query) use ($request) {
                    return $query
                        ->whereDate('date', $request->date)
                        ->where('enfant_id', $request->enfant_id)
                        ->where('user_id', $request->user_id);
                })],
            ];
            $request->validate($rules, $messages);
            $data = $request->all();
        } else {
            $messages = [
                'date.unique' => "Cet enfant a déjà un rapport à cette date",
            ];
            $rules = [
                'text' => 'required',
                'enfant_id' => 'required',
                'date' => ['required', 'date', Rule::unique('rapports')->ignore($rapport->id, 'id')->where(function ($query) use ($request) {
                    return $query
                        ->whereDate('date', $request->date)
                        ->where('enfant_id', $request->enfant_id)
                        ->where('user_id', auth()->id());
                })],
            ];
            $request->validate($rules, $messages);
            $data = $request->all();
            $data['user_id'] = auth()->id();
        }
        $rapport->update($data);
        return redirect()->route('rapports.index')->with('success', __('Rapport mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rapport  $rapport
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Rapport $rapport)
    {
        $rapport->delete();
        return redirect()->route('rapports.index')->with('success', __('Rapport supprimé'));
    }

    public function generatePdfPerDate(Request $request)
    {
        $rules = [
            'date' => 'required',
        ];
        $request->validate($rules);
        $year = Carbon::parse($request->date)->format('Y');
        $month = Carbon::parse($request->date)->format('m');
        $enfantsIds = array_unique(Rapport::atDate($year, $month)->get()->pluck('enfant_id')->toArray());
        $data = [];
        foreach($enfantsIds as $enfant_id) {
            $enfant = Enfant::find($enfant_id);
            if ($enfant) {
                $servicesArray = [];
                foreach($enfant->users as $user) {
                    $servicesArray[] = $user->service->name;
                }
                $data[$enfant->id]['enfant'] = $enfant;
                $data[$enfant->id]['date'] = $request->date;
                $data[$enfant->id]['services'] = implode(' / ', $servicesArray);
                $data[$enfant->id]['rapports'] = $enfant->rapports()->atDate($year, $month)->get();
                $data[$enfant->id]['prise'] = $enfant->prises()->atDate($year, $month)->first();
            }
        }

        $title = 'Rapports_' . Carbon::parse($request->date)->format('Y-m');
        return view('rapportpdf', compact('data', 'title'));
    }
}
