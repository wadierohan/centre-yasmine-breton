<?php

namespace App\Http\Controllers;

use App\Rdv;
use App\Prise;
use App\Enfant;
use App\Rapport;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $thisMonth = Carbon::now()->format('m');
        $thisYear = Carbon::now()->format('Y');

        $nbEnfants = Enfant::count();
        $nbPrises = Prise::atDate($thisYear, $thisMonth)->count();
        $nbRapports = Rapport::atDate($thisYear, $thisMonth)->count();
        $nbRdvs = Rdv::futur()->count();
        
        return view('home', compact('nbEnfants', 'nbPrises', 'nbRapports', 'nbRdvs'));
    }
}
