<?php

namespace App\Http\Controllers;

use App\Assurance;
use Illuminate\Http\Request;

class AssuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('assurance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('assurance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'fullname' => 'required',
            'shortname' => 'required',
        ];
        $request->validate($rules);
        $assurance = new Assurance($request->all());
        $assurance->save();
        return redirect()->route('assurances.index')->with('success', __('Assurance ajoutée'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assurance  $assurance
     *
     * @return \Illuminate\View\View
     */
    public function show(Assurance $assurance)
    {
        return view('assurance.show', ['assurance' => $assurance]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assurance  $assurance
     *
     * @return \Illuminate\View\View
     */
    public function edit(Assurance $assurance)
    {
        if ($assurance->id === 1) {
            abort(404);
        }
        return view('assurance.edit', ['assurance' => $assurance]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assurance  $assurance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Assurance $assurance)
    {
        if ($assurance->id === 1) {
            abort(404);
        }
        $rules = [
            'name' => 'required',
            'fullname' => 'required',
            'shortname' => 'required',
        ];
        $request->validate($rules);
        $assurance->update($request->all());
        return redirect()->route('assurances.index')->with('success', __('Assurance mise à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assurance  $assurance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Assurance $assurance)
    {
        if ($assurance->id === 1) {
            abort(404);
        }
        $assurance->delete();
        return redirect()->route('assurances.index')->with('success', __('Assurance supprimée'));
    }
}
