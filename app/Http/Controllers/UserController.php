<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'nullable',
            'role' => 'required|in:' . User::ROLE_ADMIN . ',' . User::ROLE_ASSISTANT,
            'password' => 'required|min:6',
            'service_id' => 'required_if:role,assistant',
        ];
        $request->validate($rules);
        $user = new User($request->all());
        $user->save();
        return redirect()->route('users.index')->with('success', __('Utilisateur ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     *
     * @return \Illuminate\View\View
     */
    public function show(User $user)
    {
        return view('user.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     *
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'phone' => 'nullable',
            'role' => 'required|in:' . User::ROLE_ADMIN . ',' . User::ROLE_ASSISTANT,
            'password' => 'nullable|min:6',
            'service_id' => 'required_if:role,assistant',
        ];
        $request->validate($rules);
        $data = $request->all();
        if (is_null($data['password'])) {
            unset($data['password']);
        }
        $user->update($data);
        return redirect()->route('users.index')->with('success', __('Utilisateur mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')->with('success', __('Utilisateur supprimé'));
    }
}
