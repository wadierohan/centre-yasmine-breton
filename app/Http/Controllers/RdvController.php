<?php

namespace App\Http\Controllers;

use App\Rdv;
use App\User;
use App\Enfant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RdvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $rdvs = Rdv::all();
        } else if (auth()->user()->isAssistant()) {
            $rdvs = auth()->user()->rdvs;
        }
        return view('rdv.index', compact('rdvs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::getAssistantsArray();
        $enfants = Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        $hours = [];
        for ($i = 6; $i < 20; $i++) {
            $hour = str_pad($i, 2, '0', STR_PAD_LEFT);
            $hours[$hour.':00'] = $hour.':00';
            $hours[$hour.':15'] = $hour.':15';
            $hours[$hour.':30'] = $hour.':30';
            $hours[$hour.':45'] = $hour.':45';
        }
        return view('rdv.create', compact('users', 'enfants', 'hours'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'enfant_id' => 'required',
            'user_id' => 'required',
            'date' => ['required', 'date'],
            'time_start' => 'date_format:H:i',
            'time_end' => 'date_format:H:i|after:time_start',
        ];
        $request->validate($rules);
        $data = $request->all();
        Rdv::create([
            'enfant_id' => $data['enfant_id'],
            'user_id' => $data['user_id'],
            'dateDebut' => Carbon::parse($data['date'].' '.$data['time_start'])->format('Y-m-d H:i:00'),
            'dateFin' => Carbon::parse($data['date'].' '.$data['time_end'])->format('Y-m-d H:i:00'),
        ]);
        return redirect()->route('rdvs.index')->with('success', __('Rendez-vous ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function show(Rdv $rdv)
    {
        $users = User::getAssistantsArray();
        return view('rdv.show', compact('rdv', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function edit(Rdv $rdv)
    {
        $users = User::getAssistantsArray();
        $enfants = Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id');
        $hours = [];
        for ($i = 6; $i < 20; $i++) {
            $hour = str_pad($i, 2, '0', STR_PAD_LEFT);
            $hours[$hour.':00'] = $hour.':00';
            $hours[$hour.':15'] = $hour.':15';
            $hours[$hour.':30'] = $hour.':30';
            $hours[$hour.':45'] = $hour.':45';
        }
        return view('rdv.edit', compact('rdv', 'users', 'enfants', 'hours'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rdv $rdv)
    {
        $rules = [
            'enfant_id' => 'required',
            'user_id' => 'required',
            'date' => ['required', 'date'],
            'time_start' => 'date_format:H:i',
            'time_end' => 'date_format:H:i|after:time_start',
        ];
        $request->validate($rules);
        $data = $request->all();
        $rdv->update([
            'enfant_id' => $data['enfant_id'],
            'user_id' => $data['user_id'],
            'dateDebut' => Carbon::parse($data['date'].' '.$data['time_start'])->format('Y-m-d H:i:00'),
            'dateFin' => Carbon::parse($data['date'].' '.$data['time_end'])->format('Y-m-d H:i:00'),
        ]);
        return redirect()->route('rdvs.index')->with('success', __('Rendez-vous mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rdv  $rdv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rdv $rdv)
    {
        $rdv->delete();
        return redirect()->route('rdvs.index')->with('success', __('Rendez-vous supprimé'));
    }

    public function showEnfantRdv(Enfant $enfant)
    {
        $events = [];
        foreach($enfant->rdvs as $rdv) {
            $events[] = [
                'title' => $rdv->user->service->name,
                'start' => $rdv->dateDebut,
                'end' => $rdv->dateFin,
            ];
        }
        return view('rdv.enfant', compact('enfant', 'events'));
    }

    public function showUserRdv(User $user)
    {
        $events = [];
        Log::debug($user->rdvs);
        foreach($user->rdvs as $rdv) {
            $events[] = [
                'title' => $rdv->enfant->fullname,
                'start' => $rdv->dateDebut,
                'end' => $rdv->dateFin,
            ];
        }
        return view('rdv.user', compact('user', 'events'));
    }
}
