<?php

namespace App\Http\Controllers;

use App\Tuteur;
use Illuminate\Http\Request;

class TuteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('tuteur.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('tuteur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'type' => 'required|in:Père,Mère,Autre',
            'email' => 'email',
        ];
        $request->validate($rules);
        $tuteur = new Tuteur($request->all());
        $tuteur->save();
        $tuteur->enfants()->sync($request->enfants);
        return redirect()->route('tuteurs.index')->with('success', __('Tuteur ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tuteur  $tuteur
     *
     * @return \Illuminate\View\View
     */
    public function show(Tuteur $tuteur)
    {
        return view('tuteur.show', ['tuteur' => $tuteur]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tuteur  $tuteur
     *
     * @return \Illuminate\View\View
     */
    public function edit(Tuteur $tuteur)
    {
        return view('tuteur.edit', ['tuteur' => $tuteur]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tuteur  $tuteur
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Tuteur $tuteur)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'type' => 'required|in:Père,Mère,Autre',
            'email' => 'email',
        ];
        $request->validate($rules);
        $tuteur->update($request->all());
        $tuteur->enfants()->sync($request->enfants);
        return redirect()->route('tuteurs.index')->with('success', __('Tuteur mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tuteur  $tuteur
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Tuteur $tuteur)
    {
        $tuteur->delete();
        return redirect()->route('tuteurs.index')->with('success', __('Tuteur supprimé'));
    }
}
