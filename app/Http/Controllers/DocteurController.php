<?php

namespace App\Http\Controllers;

use App\Docteur;
use Illuminate\Http\Request;

class DocteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('docteur.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('docteur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'email',
            'diagnostic_id' => 'required',
        ];
        $request->validate($rules);
        $docteur = new Docteur($request->all());
        $docteur->save();
        return redirect()->route('docteurs.index')->with('success', __('Docteur ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Docteur  $docteur
     *
     * @return \Illuminate\View\View
     */
    public function show(Docteur $docteur)
    {
        return view('docteur.show', ['docteur' => $docteur]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Docteur  $docteur
     *
     * @return \Illuminate\View\View
     */
    public function edit(Docteur $docteur)
    {
        return view('docteur.edit', ['docteur' => $docteur]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Docteur  $docteur
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Docteur $docteur)
    {
        $rules = [
            'name' => 'required',
            'email' => 'email',
            'diagnostic_id' => 'required',
        ];
        $request->validate($rules);
        $docteur->update($request->all());
        return redirect()->route('docteurs.index')->with('success', __('Docteur mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Docteur  $docteur
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Docteur $docteur)
    {
        $docteur->delete();
        return redirect()->route('docteurs.index')->with('success', __('Docteur supprimé'));
    }
}
