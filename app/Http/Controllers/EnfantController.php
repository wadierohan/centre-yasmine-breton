<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use App\Prise;
use App\Enfant;
use App\Tuteur;
use App\Docteur;
use App\Rapport;
use App\Service;
use App\Assistant;
use App\Assurance;
use Carbon\Carbon;
use App\Diagnostic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EnfantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $enfants = new Enfant;
        } else {
            $enfants = auth()->user()->enfants();
        }
        $perPage = request()->has('perPage') ? request('perPage') : 10;
        $queries = ['perPage' => $perPage];
        $users = User::assistant()->get();
        $userArray = [];
        foreach($users as $user) {
            $userArray[$user->id] = $user->name . ' (' . $user->service->name . ')';
        }
        $filterByUser = ['' => 'Filtrer par Utilisateur'] + $userArray;
        $filterByAssurance = ['' => 'Filtrer par assurance'] + Assurance::all(['name', 'id'])->pluck('name', 'id')->toArray();
        $orderBy = [
            '' => 'Trier par',
            'firstname' => 'Prénom',
            'lastname' => 'Nom',
            'birthdate' => 'Date de naissance',
        ];

        // Filter By user
        if (request()->has('user') && request('user')) {
            // $enfants = $enfants->where($column, request('user'));
            $user = request('user');
            $enfants = $enfants->whereHas('users', function ($query) use ($user) {
                $query->where('user_id', $user);
            });
            $queries['user'] = request('user');
        }

        // Filter By Assurance
        if (request()->has('assurance') && request('assurance')) {
            // $enfants = $enfants->where($column, request('service'));
            $assurance = request('assurance');
            $enfants = $enfants->whereHas('assurances', function ($query) use ($assurance) {
                $query->where('assurance_id', $assurance);
            });
            $queries['assurance'] = request('assurance');
        }

        // Order By
        if (request()->has('orderBy') && request()->has('sort') && request('orderBy') && request('sort')) {
            $enfants = $enfants->orderBy(request('orderBy'), request('sort'));
            $queries['orderBy'] = request('orderBy');
            $queries['sort'] = request('sort');
        } else {
            $enfants = $enfants->orderBy('firstname', 'asc')->orderBy('lastname', 'asc');
        }

        $enfants = $enfants->paginate($perPage)->appends($queries);

        return view('enfant.index', compact('enfants', 'filterByUser', 'filterByAssurance', 'orderBy', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $users = User::assistant()->get();
        $usersArray = [];
        foreach($users as $user) {
            $usersArray[$user->id] = $user->name . ' (' . $user->service->name . ')';
        }
        return view('enfant.create', compact('usersArray'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required|in:male,female',
            'birthdate' => 'required|date',
        ];
        $request->validate($rules);
        $postRequest = $request->all();
        if (substr($postRequest['diagnostic_id'], 0, 4) == 'new:') {
            $newDiagnostic = Diagnostic::create(['name' => substr($postRequest['diagnostic_id'], 4)]);
            $postRequest['diagnostic_id'] = $newDiagnostic->id;
        }
        if (substr($postRequest['docteur_id'], 0, 4) == 'new:') {
            $newDocteur = Docteur::create(['name' => substr($postRequest['docteur_id'], 4), 'diagnostic_id' => $postRequest['diagnostic_id']]);
            $postRequest['docteur_id'] = $newDocteur->id;
        }

        $enfant = new Enfant($postRequest);
        $enfant->save();
        $tuteursIds = $usersIds = $assurancesIds = [];

        // Tuteurs
        if ($request->has('tuteurs')) {
            foreach ($request->tuteurs as $tagId) {
                if (substr($tagId, 0, 4) == 'new:') {
                    $name = explode(' ', substr($tagId, 4));
                    $firstname = array_key_exists(0, $name) ? $name[0] : '';
                    $lastname = array_key_exists(1, $name) ? $name[1] : '';
                    $newTag = Tuteur::create(['firstname' => $firstname, 'lastname' => $lastname, 'type' => 'Autre']);
                    $tuteursIds[] = $newTag->id;
                    continue;
                }
                $tuteursIds[] = $tagId;
            }
        }

        // Assistants
        if ($request->has('users')) {
            foreach ($request->users as $tagId) {
                $usersIds[] = $tagId;
            }
        }

        // Assurances
        if ($request->has('assurances')) {
            foreach ($request->assurances as $tagId) {
                if (substr($tagId, 0, 4) == 'new:') {
                    $newTag = Assurance::create(['name' => substr($tagId, 4), 'fullname' => substr($tagId, 4), 'shortname' => substr($tagId, 4)]);
                    $assurancesIds[] = $newTag->id;
                    continue;
                }
                $assurancesIds[] = $tagId;
            }
        }

        $enfant->tuteurs()->sync($tuteursIds);
        $enfant->users()->sync($usersIds);
        $enfant->assurances()->sync($assurancesIds);
        $data = [];
        if ($request->has('code')) {
            foreach ($request->code as $key => $code) {
                if ($code) {
                    $date = $request->date[$key];
                    $charge = $request->charge[$key];
                    $assurance = $request->assurance[$key];
                    $data[] = [
                        'code' => $code,
                        'date' => $date,
                        'charge' => $charge,
                        'enfant_id' => $enfant->id,
                        'assurance_id' => $assurance,
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                }
            }
        }
        $prises = new Prise();
        $prises->insert($data);
        return redirect()->route('enfants.index')->with('success', __('Enfant ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enfant  $enfant
     *
     * @return \Illuminate\View\View
     */
    public function show(Enfant $enfant)
    {
        $users = User::assistant()->get();
        $usersArray = [];
        foreach($users as $user) {
            $usersArray[$user->id] = $user->name . ' (' . $user->service->name . ')';
        }
        return view('enfant.show', compact('enfant', 'usersArray'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enfant  $enfant
     *
     * @return \Illuminate\View\View
     */
    public function edit(Enfant $enfant)
    {
        $users = User::assistant()->get();
        $usersArray = [];
        foreach($users as $user) {
            $usersArray[$user->id] = $user->name . ' (' . $user->service->name . ')';
        }
        return view('enfant.edit', compact('enfant', 'usersArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enfant  $enfant
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Enfant $enfant)
    {
        if (auth()->user()->isAssistant()) {
            $data = $request->only(['anamnese']);
            $enfant->update($data);
            return redirect()->route('enfants.index')->with('success', __('Enfant mis à jour'));
        }
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required|in:male,female',
            'birthdate' => 'required|date',
        ];
        $request->validate($rules);

        $postRequest = $request->all();
        if (substr($postRequest['diagnostic_id'], 0, 4) == 'new:') {
            $newDiagnostic = Diagnostic::create(['name' => substr($postRequest['diagnostic_id'], 4)]);
            $postRequest['diagnostic_id'] = $newDiagnostic->id;
        }
        if (substr($postRequest['docteur_id'], 0, 4) == 'new:') {
            $newDocteur = Docteur::create(['name' => substr($postRequest['docteur_id'], 4), 'diagnostic_id' => $postRequest['diagnostic_id']]);
            $postRequest['docteur_id'] = $newDocteur->id;
        }

        $enfant->update($postRequest);
        $tuteursIds = $usersIds = $assurancesIds = [];

        // Tuteurs
        if ($request->has('tuteurs')) {
            foreach ($request->tuteurs as $tagId) {
                if (substr($tagId, 0, 4) == 'new:') {
                    $name = explode(' ', substr($tagId, 4));
                    $firstname = array_key_exists(0, $name) ? $name[0] : '';
                    $lastname = array_key_exists(1, $name) ? $name[1] : '';
                    $newTag = Tuteur::create(['firstname' => $firstname, 'lastname' => $lastname, 'type' => 'Autre']);
                    $tuteursIds[] = $newTag->id;
                    continue;
                }
                $tuteursIds[] = $tagId;
            }
        }

        // Users
        if ($request->has('users')) {
            foreach ($request->users as $tagId) {
                $usersIds[] = $tagId;
            }
        }

        // Assurances
        if ($request->has('assurances')) {
            foreach ($request->assurances as $tagId) {
                if (substr($tagId, 0, 4) == 'new:') {
                    $newTag = Assurance::create(['name' => substr($tagId, 4), 'fullname' => substr($tagId, 4), 'shortname' => substr($tagId, 4)]);
                    $assurancesIds[] = $newTag->id;
                    continue;
                }
                $assurancesIds[] = $tagId;
            }
        }

        $enfant->tuteurs()->sync($tuteursIds);
        $enfant->users()->sync($usersIds);
        $enfant->assurances()->sync($assurancesIds);
        $dataInsert = $dataUpdate = $dataDelete = [];
        if ($request->has('code')) {
            foreach ($request->code as $key => $code) {
                if ($code && is_array($request->prise_id) && array_key_exists($key, $request->prise_id)) {
                    $prise = Prise::find($request->prise_id[$key]);
                    if ($prise) {
                        $prise->update([
                            'code' => $code,
                            'date' => $request->date[$key],
                            'charge' => $request->charge[$key],
                            'updated_at' => \Carbon\Carbon::now(),
                        ]);
                    }
                } elseif (! $code && is_array($request->prise_id) && array_key_exists($key, $request->prise_id)) {
                    $dataDelete[] = [
                        'id' => $request->prise_id[$key],
                    ];
                } elseif ($code) {
                    $dataInsert[] = [
                        'code' => $code,
                        'date' => $request->date[$key],
                        'charge' => $request->charge[$key],
                        'enfant_id' => $enfant->id,
                        'assurance_id' => $request->assurance[$key],
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                }
            }
        }
        $prise = new Prise();
        $prise->whereIn('id', $dataDelete)->delete();
        $prise->insert($dataInsert);

        if (! is_array($request->assurances) || ! in_array('1', $request->assurances)) {
            $enfant->prises()->far()->delete();
        }

        return redirect()->route('enfants.index')->with('success', __('Enfant mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enfant  $enfant
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Enfant $enfant)
    {
        $enfant->delete();
        return redirect()->route('enfants.index')->with('success', __('Enfant supprimé'));
    }

    /**
     * Show Prises
     *
     * @param  \App\Enfant  $enfant
     *
     * @return \Illuminate\View\View
     */
    public function showPrises(Enfant $enfant)
    {
        $prises = $enfant->prises;
        return view('prise.index', ['prises' => $prises]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enfant  $enfant
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePrises(Enfant $enfant)
    {
        $prises = $enfant->prises;
        $title = 'Prises-'.$enfant->fullname;
        return view('prisepdf', compact('prises', 'title'));
    }

    public function generateRapports(Enfant $enfant)
    {
        $servicesArray = [];
        foreach($enfant->users as $user) {
            $servicesArray[] = $user->service->name;
        }
        $services = implode(' / ', $servicesArray);
        $data = [];

        $dates = array_unique($enfant->rapports->pluck('date')->toArray());
        foreach($dates as $date) {
            $year = Carbon::parse($date)->format('Y');
            $month = Carbon::parse($date)->format('m');
            $data[$year . '-' . $month]['services'] = $services;
            $data[$year . '-' . $month]['enfant'] = $enfant;
            $data[$year . '-' . $month]['date'] = $date;
            $data[$year . '-' . $month]['rapports'] = $enfant->rapports()->atDate($year, $month)->get();
            $data[$year . '-' . $month]['prise'] = $enfant->prises()->atDate($year, $month)->first();
        }

        $title = 'Rapports_' . $enfant->fullname;
        return view('rapportpdf', compact('data', 'title'));
    }

    public function generateRapport(Enfant $enfant, $date)
    {
        $year = Carbon::parse($date)->format('Y');
        $month = Carbon::parse($date)->format('m');
        $data = [];
        $servicesArray = [];
        foreach($enfant->users as $user) {
            $servicesArray[] = $user->service->name;
        }
        $data[$year . '-' . $month]['services'] = implode(' / ', $servicesArray);
        $data[$year . '-' . $month]['enfant'] = $enfant;
        $data[$year . '-' . $month]['date'] = $date;
        $data[$year . '-' . $month]['rapports'] = $enfant->rapports()->atDate($year, $month)->get();
        $data[$year . '-' . $month]['prise'] = $enfant->prises()->atDate($year, $month)->first();

        $title = 'Rapport_' . Carbon::parse($date)->format('Y-m') . '_' . $enfant->fullname;
        return view('rapportpdf', compact('data', 'title'));
    }
}
