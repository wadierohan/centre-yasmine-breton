<?php

namespace App\Http\Controllers;

use App\Diagnostic;
use Illuminate\Http\Request;

class DiagnosticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('diagnostic.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('diagnostic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $diagnostic = new Diagnostic($request->all());
        $diagnostic->save();
        return redirect()->route('diagnostics.index')->with('success', __('Diagnostic ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Diagnostic  $diagnostic
     *
     * @return \Illuminate\View\View
     */
    public function show(Diagnostic $diagnostic)
    {
        return view('diagnostic.show', ['diagnostic' => $diagnostic]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diagnostic  $diagnostic
     *
     * @return \Illuminate\View\View
     */
    public function edit(Diagnostic $diagnostic)
    {
        return view('diagnostic.edit', ['diagnostic' => $diagnostic]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diagnostic  $diagnostic
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Diagnostic $diagnostic)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $diagnostic->update($request->all());
        return redirect()->route('diagnostics.index')->with('success', __('Diagnostic mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diagnostic  $diagnostic
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Diagnostic $diagnostic)
    {
        $diagnostic->delete();
        return redirect()->route('diagnostics.index')->with('success', __('Diagnostic supprimé'));
    }
}
