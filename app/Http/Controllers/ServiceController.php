<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('service.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $service = new Service($request->all());
        $service->save();
        return redirect()->route('services.index')->with('success', __('Service ajouté'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     *
     * @return \Illuminate\View\View
     */
    public function show(Service $service)
    {
        return view('service.show', ['service' => $service]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     *
     * @return \Illuminate\View\View
     */
    public function edit(Service $service)
    {
        return view('service.edit', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Service $service)
    {
        $rules = [
            'name' => 'required',
        ];
        $request->validate($rules);
        $inputs = $request->all();
        if (!$request->has('tva')) {
            $inputs['tva'] = false;
        }
        $service->update($inputs);
        return redirect()->route('services.index')->with('success', __('Service mis à jour'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect()->route('services.index')->with('success', __('Service supprimé'));
    }
}
