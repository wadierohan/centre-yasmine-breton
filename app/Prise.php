<?php

namespace App;

use NumberFormatter;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Prise
 *
 * @property int $id
 * @property string $code
 * @property string $date
 * @property int $enfant_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $assurance_id
 * @property float $charge
 * @property string $facture
 * @property string $facture_date
 * @property-read \App\Assurance $assurance
 * @property-read \App\Enfant $enfant
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise far()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereAssuranceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereEnfantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereFacture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereFactureDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Prise whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Prise extends Model
{
    protected $fillable = [
        'code', 'date', 'enfant_id', 'assurance_id', 'charge', 'facture', 'service_id', 'facture_year', 'facture_date',
    ];

    protected $appends = ['wordCharge'];

    public function getWordChargeAttribute() {
        $f = new NumberFormatter("fr", NumberFormatter::SPELLOUT);
        return $f->format($this->charge);
    }

    public function enfant()
    {
        return $this->belongsTo('App\Enfant');
    }

    public function assurance()
    {
        return $this->belongsTo('App\Assurance');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function scopeFar($query)
    {
        return $query->where('assurance_id', 1);
    }

    public function scopeAtDate($query, $year, $month)
    {
        return $query->whereYear('date', $year)->whereMonth('date', $month);
    }
}
