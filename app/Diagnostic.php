<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Diagnostic
 *
 * @property int $id
 * @property string $name
 * @property string|null $desc
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Docteur[] $docteurs
 * @property-read int|null $docteurs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Enfant[] $enfants
 * @property-read int|null $enfants_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Diagnostic whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Diagnostic extends Model
{
    protected $fillable = [
        'name', 'desc',
    ];

    public function enfants()
    {
        return $this->hasMany('App\Enfant');
    }

    public function docteurs()
    {
        return $this->hasMany('App\Docteur');
    }
}
