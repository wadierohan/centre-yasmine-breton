<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Assistant
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Enfant[] $enfants
 * @property-read int|null $enfants_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assistant whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Assistant extends Model
{
    protected $fillable = [
        'name', 'email', 'phone',
    ];

    public function enfants()
    {
        return $this->belongsToMany('App\Enfant');
    }
}
