<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Docteur
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $phone
 * @property int $diagnostic_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Diagnostic $diagnostic
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Enfant[] $enfants
 * @property-read int|null $enfants_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur whereDiagnosticId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docteur whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Docteur extends Model
{
    protected $fillable = [
        'name', 'email', 'phone', 'diagnostic_id',
    ];

    public function enfants()
    {
        return $this->hasMany('App\Enfant');
    }

    public function diagnostic()
    {
        return $this->belongsTo('App\Diagnostic');
    }
}
