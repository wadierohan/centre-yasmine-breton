<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tuteur
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $type
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Enfant[] $enfants
 * @property-read int|null $enfants_count
 * @property-read mixed $fullname
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tuteur whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Tuteur extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'type', 'email', 'phone',
    ];

    public function getFullnameAttribute($value)
    {
        return ucfirst($this->firstname) . ' ' . ucfirst($this->lastname);
    }

    public function enfants()
    {
        return $this->belongsToMany('App\Enfant');
    }

    public function delete()
    {
        $this->enfants()->detach();
        parent::delete();
    }
}
