<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'name' => 'Orthophonie',
                'tva' => false,
            ],
            [
                'name' => 'Psychomotricite',
                'tva' => false,
            ],
            [
                'name' => 'Kinésithérapie',
                'tva' => true,
            ],
            [
                'name' => 'Education spécialisee en ABA',
                'tva' => true,
            ],
            [
                'name' => 'Coaching ABA',
                'tva' => true,
            ],
        ]);
    }
}
