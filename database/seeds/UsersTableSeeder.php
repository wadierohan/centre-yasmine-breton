<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Administrateur',
                'email' => 'admin@centreyasmine.com',
                'password' => bcrypt('123456'),
                'role' => 'admin',
                'service_id' => null,
                'phone' => null,
            ],
            [
                'name' => 'Assistante1',
                'email' => 'assistant1@centreyasmine.com',
                'password' => bcrypt('123456'),
                'role' => 'assistant',
                'service_id' => 1,
                'phone' => '0655664499',
            ],
            [
                'name' => 'Assistante2',
                'email' => 'assistant2@centreyasmine.com',
                'password' => bcrypt('123456'),
                'role' => 'assistant',
                'service_id' => 2,
                'phone' => '0655664499',
            ],
        ]);
    }
}
