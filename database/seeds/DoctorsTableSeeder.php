<?php

use Illuminate\Database\Seeder;

class DocteursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('docteurs')->insert([
            [
                'name' => 'Docteur 1',
                'email' => 'docteur1@centreyasmine.com',
                'phone' => '0655447788',
                'diagnostic_id' => 1,
            ],
            [
                'name' => 'Docteur 2',
                'email' => 'docteur2@centreyasmine.com',
                'phone' => '0655447789',
                'diagnostic_id' => 2,
            ],
        ]);
    }
}
