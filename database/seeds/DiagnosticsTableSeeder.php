<?php

use Illuminate\Database\Seeder;

class DiagnosticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diagnostics')->insert([
            [
                'name' => 'Retard de language',
            ],
            [
                'name' => 'Retard psychomoteur',
            ],
        ]);
    }
}
