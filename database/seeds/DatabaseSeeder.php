<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AssurancesTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(DiagnosticsTableSeeder::class);
        $this->call(DocteursTableSeeder::class);
        $this->call(EnfantsTableSeeder::class);
    }
}
