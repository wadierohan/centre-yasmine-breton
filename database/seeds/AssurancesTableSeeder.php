<?php

use Illuminate\Database\Seeder;

class AssurancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assurances')->insert([
            [
                'name' => 'FAR',
                'fullname' => 'Forces armées royales',
                'shortname' => 'FAR',
            ],
            [
                'name' => 'CNSS',
                'fullname' => 'CNSS',
                'shortname' => 'CNSS',
            ],
        ]);
    }
}
