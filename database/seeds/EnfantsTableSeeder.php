<?php

use Illuminate\Database\Seeder;

class EnfantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enfants')->insert([
            [
                'firstname' => 'prénom 1',
                'lastname' => 'nom 1',
                'gender' => 'male',
                'birthdate' => '2010-06-05',
                'remarque' => '',
                'diagnostic_id' => 1,
                'docteur_id' => 1,
                'anamnese' => 'Anamnèse enfant 1',
            ],
            [
                'firstname' => 'prénom 2',
                'lastname' => 'nom 2',
                'gender' => 'female',
                'birthdate' => '2011-10-12',
                'remarque' => '',
                'diagnostic_id' => 2,
                'docteur_id' => 2,
                'anamnese' => 'Anamnèse enfant 2',
            ],
        ]);
    }
}
