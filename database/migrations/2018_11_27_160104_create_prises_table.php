<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prises', function (Blueprint $table) {
            //'code', 'date', 'enfant_id'
            $table->increments('id');
            $table->string('code');
            $table->date('date');
            $table->integer('enfant_id');
            $table->timestamps();
            $table->unique(['date', 'enfant_id'], 'unique_prise_en_charge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prises');
    }
}
