include .make/tools.mk
include .make/artisan.mk
include .make/provision.mk
include .make/analyze.mk
include .make/composer.mk
include .make/docker-compose.mk