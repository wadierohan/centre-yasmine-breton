.PHONY: help
help::
	@printf "\n"
	@printf "\033[33m Analyze commands:\033[39m\n"
	@printf "\033[38;5;106m   phpstan                              \033[39m   Run PHPStan\n"
	@printf "\033[38;5;106m   phpunit                              \033[39m   Run PHPUnit\n"
	@printf "\033[38;5;106m   php-cs                               \033[39m   Run PHP-CS check only\n"
	@printf "\033[38;5;106m   php-cs-fixer                         \033[39m   Run PHP-CS-FIXER Fix errors\n"
	@printf "\033[38;5;106m   phpmetrics                           \033[39m   Produces a report and metrics\n"

.PHONY: phpstan
phpstan::
	$(call run, \
		$(RUN_PHPSTAN) analyse \
	)

.PHONY: php-cs-fixer
php-cs-fixer::
	$(call run, \
		$(DOCKER_COMPOSE_RUN_PHP) ./vendor/bin/php-cs-fixer fix \
	)

.PHONY: php-cs
php-cs::
	$(call run, \
		$(DOCKER_COMPOSE_RUN_PHP) ./vendor/bin/php-cs-fixer fix --dry-run \
	)

.PHONY: phpmetrics
phpmetrics::
	$(call run, \
		$(DOCKER_COMPOSE_RUN_PHP) ./vendor/bin/phpmetrics --report-html="./webroot/reports" src \
	)


.PHONY: phpunit
phpunit::
	$(call run, \
		$(DOCKER_COMPOSE_RUN_PHP) ./vendor/bin/phpunit \
	)
