.PHONY: help
help::
	@printf "\n"
	@printf "\033[33m Project:\033[39m\n"
	@printf "\033[38;5;106m   bash-php     \033[39m                      Connect to TTY of php container.\n"
	@printf "\033[38;5;106m   bash-db      \033[39m                      Connect to TTY of mysql container.\n"
	@printf "\n"

.PHONY: bash-php
bash-php:: run
	$(call run, \
		$(DOCKER_COMPOSE) exec php bash \
	)

.PHONY: bash-db
bash-db:: run
	$(call run, \
		$(DOCKER_COMPOSE) exec db bash \
	)
