.PHONY: help
help::
	@printf "\n"
	@printf "\033[33m Laravel artisan commands:\033[39m\n"
	@printf "\033[38;5;106m   artisan                             \033[39m   Display artisan commands\n"
	@printf "\033[38;5;106m   artisan-key                         \033[39m   Artisan key generate\n"
	@printf "\033[38;5;106m   artisan-passport                    \033[39m   Artisan passport install\n"
	@printf "\033[38;5;106m   artisan-migrate                     \033[39m   Artisan migrate\n"
	@printf "\033[38;5;106m   artisan-migrate-seed                \033[39m   Artisan migrate and seed\n"
	@printf "\033[38;5;106m   artisan-route                       \033[39m   Artisan route list\n"
	@printf "\033[38;5;106m   artisan-cache-clear                 \033[39m   Artisan clear cache\n"
	@printf "\033[38;5;106m   artisan-config-clear                \033[39m   Artisan clear config\n"
	@printf "\033[38;5;106m   artisan-route-clear                 \033[39m   Artisan clear route\n"
	@printf "\033[38;5;106m   artisan-storage-link                \033[39m   Artisan generate public link\n"
	@printf "\033[38;5;106m   artisan-jwt-secret                  \033[39m   Artisan generate jwt secret key\n"

.PHONY: artisan
artisan::
	$(call run, \
		$(RUN_ARTISAN) \
	)

.PHONY: artisan-key
artisan-key::
	$(call run, \
		$(RUN_ARTISAN) key:generate \
	)

.PHONY: artisan-passport
artisan-passport::
	$(call run, \
		$(RUN_ARTISAN) passport:install \
	)

.PHONY: artisan-migrate
artisan-migrate::
	$(call run, \
		$(RUN_ARTISAN) migrate --force \
	)

.PHONY: artisan-migrate-seed
artisan-migrate-seed::
	$(call run, \
		$(RUN_ARTISAN) migrate:fresh --seed \
	)

.PHONY: artisan-route
artisan-route::
	$(call run, \
		$(RUN_ARTISAN) route:list \
	)

.PHONY: artisan-cache-clear
artisan-cache-clear::
	$(call run, \
		$(RUN_ARTISAN) cache:clear \
	)

.PHONY: artisan-config-clear
artisan-config-clear::
	$(call run, \
		$(RUN_ARTISAN) config:clear \
	)

.PHONY: artisan-route-clear
artisan-route-clear::
	$(call run, \
		$(RUN_ARTISAN) route:clear \
	)

.PHONY: artisan-storage-link
artisan-storage-link::
	$(call run, \
		$(RUN_ARTISAN) storage:link \
	)

.PHONY: artisan-jwt-secret
artisan-jwt-secret::
	$(call run, \
		$(RUN_ARTISAN) jwt:secret \
	)
