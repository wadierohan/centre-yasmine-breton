.PHONY: help
help::
	@printf "\n"
	@printf "\033[33m Composer commands:\033[39m\n"
	@printf "\033[38;5;106m   composer                            \033[39m   Prints the command to execute composer commands\n"
	@printf "\033[38;5;106m   composer-install                    \033[39m   Install composer dependencies. Should be done automatically\n"
	@printf "\033[38;5;106m   composer-validate                   \033[39m   Validate composer dependencies\n"

# If composer.json has changed, you need to update composer.lock
composer.lock: composer.json
	@if [ -f $@ ]; \
	then\
		printf "\n";\
		printf "  \033[38;5;208m    The $< file has changed. Synchronizing vendor    \033[39m\n";\
		printf "\n";\
		$(MAKE) -s composer-install;\
		touch composer.lock;\
	else\
		$(MAKE) -s composer-update;\
	fi

vendor: composer.lock
# Only executes if the composer.lock did not already
	@if ! [ -d vendor ]; then $(MAKE) -s composer-install; fi

.PHONY: composer
# show the composer command to easily copy/paste it and run special commands
composer::
	$(call run, \
		$(RUN_COMPOSER) \
	)

.PHONY: composer-install
composer-install::
	$(call run, \
		$(RUN_COMPOSER) install --no-progress --prefer-dist --no-suggest \
	)

.PHONY: composer-validate
composer-validate::
	$(call run, \
		$(RUN_COMPOSER) validate \
	)
