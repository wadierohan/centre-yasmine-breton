define silent_run
@$(1)
endef

define run
@printf "\n    \033[38;5;106m $(1)\033[39m\n"
$(call silent_run, $(1))
endef

define copy-dist-file
@if [ -f $@ ] && ! diff $< $@ >/dev/null 2>&1; \
then\
	printf "\n    \033[38;5;208mThe $< file has changed. Please check your $@ file\033[39m\n\n";\
	touch $@;\
	printf "\n    \033[38;5;208mdiff $< $@\033[39m\n\n";\
								diff $< $@;\
	exit 1;\
else\
	printf "\n    \033[38;5;106mcp $< $@\033[39m\n\n";\
								cp $< $@;\
fi
endef

-include .make/.env.example .make/.env

ifeq ($(strip $(DOCKER_ENV)),)
DOCKER_COMPOSE_FILE := docker-compose.yml
else
DOCKER_COMPOSE_FILE := docker-compose.$(DOCKER_ENV).yml
endif

DOCKER_COMPOSE := docker-compose -p $(DOCKER_PROJECT) -f $(DOCKER_COMPOSE_FILE)

DOCKER_COMPOSE_RUN ?= $(DOCKER_COMPOSE) run -u 1000 -T --rm
DOCKER_COMPOSE_RUN_PHP ?= $(DOCKER_COMPOSE_RUN) php
RUN_PHPSTAN ?= $(DOCKER_COMPOSE_RUN_PHP) php -d memory_limit=-1 ./vendor/bin/phpstan
RUN_PHPCSFIXER ?= $(DOCKER_COMPOSE_RUN_PHP) ./vendor/bin/php-cs-fixer --config=.php_cs
RUN_COMPOSER ?= $(DOCKER_COMPOSE_RUN_PHP) composer --ansi
RUN_ARTISAN ?= $(DOCKER_COMPOSE_RUN_PHP) php artisan
