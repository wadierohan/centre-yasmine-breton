<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('users', 'UserController');
    Route::resource('assurances', 'AssuranceController');
    Route::resource('diagnostics', 'DiagnosticController');
    Route::resource('docteurs', 'DocteurController');
    Route::resource('enfants', 'EnfantController');
    Route::resource('prises', 'PriseController');
    Route::resource('services', 'ServiceController');
    Route::resource('tuteurs', 'TuteurController');
    Route::resource('rapports', 'RapportController');
    Route::resource('rdvs', 'RdvController');

    Route::group(['prefix' => 'prises/{prise}'], function () {
        Route::get('generatePdf', 'PriseController@generatePdf')->name('prises.generatePdf');
    });

    Route::post('rapports/generatePdfPerDate', 'RapportController@generatePdfPerDate')->name('rapports.generatePdfPerDate');

    Route::group(['prefix' => 'enfants/{enfant}'], function () {
        Route::get('prises', 'EnfantController@showPrises')->name('enfants.showPrises');
        Route::get('generatePrises', 'EnfantController@generatePrises')->name('enfants.generatePrises');
        Route::get('generateRapports', 'EnfantController@generateRapports')->name('enfants.generateRapports');
        Route::get('generateRapport/{date}', 'EnfantController@generateRapport')->name('enfants.generateRapport');
    });

    Route::group(['prefix' => 'rdvs'], function () {
        Route::get('enfant/{enfant}', 'RdvController@showEnfantRdv')->name('rdvs.showEnfantRdv');
        Route::get('assistant/{user}', 'RdvController@showUserRdv')->name('rdvs.showUserRdv');
    });

    Route::get('rapports/{rapport}/duplicate', 'RapportController@duplicate')->name('rapports.duplicate');
});
