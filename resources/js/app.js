
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.toastr = require('toastr');
window.sweetalert = require('sweetalert');
window.datetimepicker = require('jquery-datetimepicker');
window.select2 = require('select2');

window.toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "linear",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

window.$('select.tags').select2({
    theme: "bootstrap",
    closeOnSelect: false,
    width: 'style',
    tags: true,
    tokenSeparators: [','],
    createTag: function (params) {
        var term = $.trim(params.term);
    
        if (term === '') {
            return null;
        }
    
        return {
            id: 'new:' + term,
            text: term
        };
    }
});

window.$('select:not(.tags)').select2({
    theme: "bootstrap",
    closeOnSelect: true,
    width: 'style'
});

window.$.datetimepicker.setLocale('fr');
window.$('.datepicker').datetimepicker({
    i18n:{
        fr:{
            months:[
                'Janvier','Février','Mars','Avril',
                'Mai','Juin','Juillet','Août',
                'Septembre','Octobre','Novembre','Decembre',
            ],
            dayOfWeek:[
                "Dim", "Lun", "Mar", "Mer", 
                "Jeu", "Ven", "Sam",
            ]
        }
    },
    timepicker:false,
    format:'Y-m-d'
});

window.$('.delete-model').click(function(){
    var that = $(this);
    swal({   
        title: "Attention!",
        text: "Êtes vous sûr de vouloir supprimer cela?", 
        icon: "warning",   
        buttons: ["Non", "Oui"],
        dangerMode: true,
    }).then((confirmed) => {
        if(confirmed)
            that.closest('form').submit();
        else
            return;
    });
});

require('./custom');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*
const app = new Vue({
    el: '#app'
});
*/