$(function(){
    $('.mymonthpicker, .myyearpicker').change(function(){
        var month = $(this).closest('.form-row').find('.mymonthpicker').val();
        var year = $(this).closest('.form-row').find('.myyearpicker').val();
        $(this).closest('.form-row').find('.mydate').val(year+'-'+month+'-01');
    });
    $('.assurances').on('select2:select', function (e) {
        if(e.params.data.id == 1){
            $('.far-options').show();
        }
    });
    $('.assurances').on('select2:unselect', function (e) {
        if(e.params.data.id == 1){
            $('.far-options').hide();
        }
    });
})