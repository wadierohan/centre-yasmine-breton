@extends('layouts.app')

@section('content')
<div class="container-fluid">
    {{ Form::open([ 'method'  => 'patch', 'route' => [ 'enfants.update', $enfant->id ], 'autocomplete' => 'off', 'files' => true ]) }}
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    {{ $enfant->fullname }}
                </div>

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            {!! Form::label('firstname', __('Prénom'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('firstname', $enfant->firstname, ['class' => 'form-control'.($errors->has('firstname') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('firstname') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('lastname', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('lastname', $enfant->lastname, ['class' => 'form-control'.($errors->has('lastname') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('lastname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('lastname') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('gender', __('Sexe'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('gender', ['male' => __('Mâle'), 'female' => __('Femelle') ], $enfant->gender, ['class' => 'form-control'.($errors->has('gender') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('gender'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('gender') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('birthdate', __('Date de naissance'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('birthdate', $enfant->birthdate, ['class' => 'datepicker form-control'.($errors->has('birthdate') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('birthdate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('birthdate') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            {!! Form::label('tuteurs', __('Tuteurs'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('tuteurs[]', App\Tuteur::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id'), $enfant->tuteurs, ['class' => 'tags form-control'.($errors->has('tuteurs') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('tuteurs'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('tuteurs') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('users', __('Suivi par'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('users[]', $usersArray, $enfant->users, ['class' => 'tags form-control'.($errors->has('users') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('users'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('users') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('assurances', __('Assurances'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('assurances[]', App\Assurance::all(['id', 'name'])->pluck('name', 'id'), $enfant->assurances, ['class' => 'tags form-control assurances'.($errors->has('assurances') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                            @if ($errors->has('assurances'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('assurances') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('diagnostic_id', __('Diagnostic'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('diagnostic_id', App\Diagnostic::all(['id', 'name'])->pluck('name', 'id'), $enfant->diagnostic_id, ['class' => 'tags form-control'.($errors->has('diagnostic_id') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('diagnostic_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('diagnostic_id') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('docteur_id', __('Diagnostiqué par'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('docteur_id', App\Docteur::all(['id', 'name'])->pluck('name', 'id'), $enfant->docteur_id, ['class' => 'tags form-control'.($errors->has('docteur_id') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('docteur_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('docteur_id') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('remarque', __('Remarque'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::textarea('remarque', $enfant->remarque, ['class' => 'form-control'.($errors->has('remarque') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('remarque'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('remarque') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('anamnese', __('Anamnèse'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::textarea('anamnese', $enfant->anamnese, ['class' => 'form-control'.($errors->has('anamnese') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('anamnese'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('anamnese') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 prise-options far-options" style="{{ !$enfant->prises()->far()->exists() ? 'display:none;' : '' }}">
            <div class="card">
                <div class="card-header">{{ __('Prises en charge') }}</div>
                <div class="card-body">
                    @php $j = 0 @endphp
                    @foreach($enfant->prises()->far()->get() as $prise)
                        @php $j++ @endphp
                        <div class="form-row">
                            <div class="form-group col-3">
                                {!! Form::label('code', __('Code'), ['class' => 'col-form-label text-md-right']) !!}
                                {!! Form::text('code[]', $prise->code, ['class' => 'form-control'.($errors->has('code') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('code') !!}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-3">
                                {!! Form::label('charge', __('Charge'), ['class' => 'col-form-label text-md-right']) !!}
                                {!! Form::number('charge[]', $prise->charge, ['class' => 'form-control'.($errors->has('charge') ? ' is-invalid' : '')]) !!}
                                @if ($errors->has('charge'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('charge') !!}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-3">
                                {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                                {!! Form::selectMonth('month', \Carbon\Carbon::parse($prise->date)->month, ['class' => 'mymonthpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                            </div>
                            <div class="form-group col-3">
                                {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                                {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::parse($prise->date)->year, ['class' => 'myyearpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                            </div>
                            {!! Form::hidden('date[]', \Carbon\Carbon::parse($prise->date)->format('Y-m-01'), ['class' => 'mydate']) !!}
                            {!! Form::hidden('assurance[]', 1) !!}
                            {!! Form::hidden('prise_id[]', $prise->id) !!}
                            {{--<div class="form-group col-2 col-sm-1 col-md-3 col-lg-2 col-xl-2">
                                <button type="button" class="btn btn-danger delete-row">{{__('Supp')}}</button>
                            </div>--}}
                        </div>
                    @endforeach
                    @for($i=$j; $i<12; $i++)
                    <div class="form-row">
                        <div class="form-group col-3">
                            {!! Form::label('code', __('Code'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('code[]', null, ['class' => 'form-control'.($errors->has('code') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('code') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-3">
                            {!! Form::label('charge', __('Charge'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::number('charge[]', null, ['class' => 'form-control'.($errors->has('charge') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('charge'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('charge') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-3">
                            {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectMonth('month', \Carbon\Carbon::now()->month, ['class' => 'mymonthpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                        </div>
                        <div class="form-group col-3">
                            {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::now()->year, ['class' => 'myyearpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                        </div>
                        {!! Form::hidden('date[]', \Carbon\Carbon::now()->format('Y-m-01'), ['class' => 'mydate']) !!}
                        {!! Form::hidden('assurance[]', 1) !!}
                        {{--<div class="form-group col-2 col-sm-1 col-md-3 col-lg-2 col-xl-2">
                            <button type="button" class="btn btn-danger delete-row">{{__('Supp')}}</button>
                        </div>--}}
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@endsection
