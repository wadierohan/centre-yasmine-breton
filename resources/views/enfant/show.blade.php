@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    {{ $enfant->fullname }}
                </div>

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            {!! Form::label('firstname', __('Prénom'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('firstname', $enfant->firstname, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('lastname', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('lastname', $enfant->lastname, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('gender', __('Sexe'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('gender', ['male' => __('Mâle'), 'female' => __('Femelle') ], $enfant->gender, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::label('birthdate', __('Date de naissance'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('birthdate', $enfant->birthdate, ['class' => 'datepicker form-control', 'readonly' => 'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            {!! Form::label('tuteurs', __('Tuteurs'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('tuteurs[]', App\Tuteur::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id'), $enfant->tuteurs, ['class' => 'tags form-control', 'disabled' => 'disabled', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('users', __('Suivi par'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('users[]', $usersArray, $enfant->users, ['class' => 'tags form-control', 'disabled' => 'disabled', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('assurances', __('Assurances'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('assurances[]', App\Assurance::all(['id', 'name'])->pluck('name', 'id'), $enfant->assurances, ['class' => 'tags form-control assurances', 'disabled' => 'disabled', 'multiple' => 'multiple']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('diagnostic', __('Diagnostic'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('diagnostic_id', App\Diagnostic::all(['id', 'name'])->pluck('name', 'id'), $enfant->diagnostic_id, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('docteur', __('Diagnostiqué par'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('docteur_id', App\Docteur::all(['id', 'name'])->pluck('name', 'id'), $enfant->docteur_id, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('remarque', __('Remarque'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::textarea('remarque', $enfant->remarque, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('anamnese', __('Anamnèse'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::textarea('anamnese', $enfant->anamnese, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 prise-options far-options" style="{{ !$enfant->prises()->far()->exists() ? 'display:none;' : '' }}">
            <div class="card">
                <div class="card-header">{{ __('Prises en charge') }}</div>
                <div class="card-body">
                    @foreach($enfant->prises()->far()->get() as $prise)
                        <div class="form-row">
                            <div class="form-group col-3">
                                {!! Form::label('code', __('Code'), ['class' => 'col-form-label text-md-right']) !!}
                                {!! Form::text('code[]', $prise->code, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                            </div>
                            <div class="form-group col-3">
                                {!! Form::label('charge', __('Charge'), ['class' => 'col-form-label text-md-right']) !!}
                                {!! Form::number('charge[]', $prise->charge, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                            </div>
                            <div class="form-group col-3">
                                {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                                {!! Form::selectMonth('month', \Carbon\Carbon::parse($prise->date)->month, ['class' => 'mymonthpicker form-control', 'disabled' => 'disabled']) !!}
                            </div>
                            <div class="form-group col-3">
                                {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                                {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::parse($prise->date)->year, ['class' => 'myyearpicker form-control', 'disabled' => 'disabled']) !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
