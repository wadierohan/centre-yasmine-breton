@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Enfants') }}
                    @if(auth()->user()->isAdmin())
                    <a class="btn btn-primary float-sm-right" href="{{route('enfants.create')}}" role="button">{{__('Ajouter')}}</a>
                    @endif
                </div>

                <div class="card-body">
                    @component('components.exportrapports')@endcomponent
                    @component('components.filters.enfants', compact('orderBy', 'filterByUser', 'filterByAssurance', 'perPage'))@endcomponent
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Prénom') }}</th>
                                <th>{{ __('Sexe') }}</th>
                                <th>{{ __('Date de naissance') }}</th>
                                <th>{{ __('Tuteurs') }}</th>
                                <th>{{ __('Assistants') }}</th>
                                <th>{{ __('Assurances') }}</th>
                                <th>{{ __('Remarque') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="9">{{ $enfants->links() }}</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($enfants as $enfant)
                            <tr>
                                <td>{{ $enfant->lastname }}</td>
                                <td>{{ $enfant->firstname }}</td>
                                <td>{{ $enfant->gender }}</td>
                                <td>{{ \Carbon\Carbon::parse($enfant->birthdate)->format('d/m/Y') }}</td>
                                <td>
                                    @foreach($enfant->tuteurs as $tuteur)
                                    <span class="badge badge-light">{{ $tuteur->fullname }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($enfant->users as $user)
                                    <span class="badge badge-light">{{ $user->name }} ({{ $user->service->name }})</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($enfant->assurances as $assurance)
                                    <span class="badge badge-light">{{ $assurance->name }}</span>
                                    @endforeach
                                </td>
                                <td>{{ $enfant->remarque ? __('Oui') : __('Non') }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$enfant->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$enfant->id}}">
                                            <a class="dropdown-item" href="{{route('enfants.show', $enfant->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('enfants.edit', $enfant->id)}}">{{__('Modifier')}}</a>
                                            <div class="dropdown-divider"></div>
                                            @if(auth()->user()->isAdmin() && $enfant->prises->count() > 0)
                                            <a class="dropdown-item" target="_blank" href="{{route('enfants.showPrises', $enfant->id)}}">{{__('Voir les prises')}}</a>
                                            <a class="dropdown-item" target="_blank" href="{{route('enfants.generatePrises', $enfant->id)}}">{{__('Exporter les Prises')}}</a>
                                            <div class="dropdown-divider"></div>
                                            @endif
                                            @if(auth()->user()->isAdmin() && $enfant->rapports->count() > 0)
                                            <a class="dropdown-item" target="_blank" href="{{route('enfants.generateRapports', $enfant->id)}}">{{__('Exporter tout les rapports')}}</a>
                                            <li class="dropdown-submenu">
                                                <a href="#" class="dropdown-item dropdown-toggle"><span class="nav-label">{{__('Les rapports')}}</span><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    @foreach(array_unique($enfant->rapports->pluck('date')->toArray()) as $date)
                                                    <a class="dropdown-item" target="_blank" href="{{route('enfants.generateRapport', ['enfant' => $enfant->id, 'date' => $date])}}">{{ $date }}</a>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            <!--array_unique($enfant->rapports->pluck('date')->toArray())-->
                                            <div class="dropdown-divider"></div>
                                            @endif
                                            @if($enfant->rdvs->count() > 0)
                                            <a class="dropdown-item" href="{{route('rdvs.showEnfantRdv', $enfant->id)}}">{{__('Voir les rendez-vous')}}</a>
                                            <div class="dropdown-divider"></div>
                                            @endif
                                            @if(auth()->user()->isAdmin())
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'enfants.destroy', $enfant->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
