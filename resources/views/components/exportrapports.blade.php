<div class="export-rapports">
    {{ Form::open([ 'method'  => 'POST', 'route' => [ 'rapports.generatePdfPerDate' ], 'autocomplete' => 'off', 'target' => '_blank']) }}
        <div class="form-row">
            <div class="form-group col-md-4">
                {!! Form::selectMonth('month', \Carbon\Carbon::now()->month, ['class' => 'mymonthpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
            </div>
            <div class="form-group col-md-4">
                {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::now()->year, ['class' => 'myyearpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
            </div>
            {!! Form::hidden('date', \Carbon\Carbon::now()->format('Y-m-01'), ['class' => 'mydate']) !!}
            @if ($errors->has('date'))
                <span class="invalid-feedback" role="alert">
                    <strong>{!! $errors->first('date') !!}</strong>
                </span>
            @endif
            <div class="form-group col-md-4">
                {!! Form::submit(__('Export rapports'), ['class' => 'btn btn-block btn-success']) !!}
            </div>
        </div>
    {{ Form::close() }}
</div>
