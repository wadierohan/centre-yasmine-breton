<div class="filter">
    {{ Form::open([ 'method'  => 'GET', 'route' => [ 'rapports.index' ], 'autocomplete' => 'off']) }}
        <table class="table">
            <thead>
                <tr>
                    <th>{{ __('Eléments par page') }}</th>
                    <th>{{ __('Filtres') }}</th>
                    <th>{{ __('Ordre') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="form-group">
                            {!! Form::select('perPage', ['' => 'Tous', '10' => '10', '20' => '20', '50' => '50', '100' => '100'], $perPage, ['class' => 'form-control']) !!}
                        </div>
                    </td>
                    <td>
                        @if (auth()->user()->isAdmin())
                        <div class="form-group">
                            {!! Form::select('user', $filterByUser, request('user'), ['class' => 'form-control']) !!}
                        </div>
                        @endif
                        <div class="form-group">
                            {!! Form::select('enfant', $filterByEnfant, request('enfant'), ['class' => 'form-control']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            {!! Form::select('orderBy', $orderBy, request('orderBy'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::select('sort', ['asc' => 'Ascendant', 'desc' => 'Descendant'], request('sort'), ['class' => 'form-control']) !!}
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning btn-block">{{__('Appliquer')}}</button>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    {{ Form::close() }}
</div>
