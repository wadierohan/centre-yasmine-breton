@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vérifiez votre adresse email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un lien de réinitialisation de mot de passe a été envoyé à votre adresse email.') }}
                        </div>
                    @endif

                    {{ __('Avant de continuer, merci de vérifier votre adresse email pour le lien de réinitialisation du mot de passe.') }}
                    {{ __('Si vous n'avez reçu aucun email') }}, <a href="{{ route('verification.resend') }}">{{ __('cliquez ici pour demander un autre.') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
