@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Rapport') }} - {{ \Carbon\Carbon::parse($rapport->date)->format('m/Y') }} - {{ $rapport->enfant->fullname }} - {{ $rapport->user->name }} ({{ $rapport->user->service->name }})</div>

                <div class="card-body">
                    {{ Form::open([ 'method'  => 'patch', 'route' => [ 'rapports.update', $rapport->id ], 'autocomplete' => 'off', 'files' => true ]) }}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectMonth('month', \Carbon\Carbon::parse($rapport->date)->month, ['class' => 'mymonthpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('date') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::parse($rapport->date)->year, ['class' => 'myyearpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                        </div>
                        {!! Form::hidden('date', $rapport->date, ['class' => 'mydate']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('enfant_id', __('Enfant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfant_id', $enfants, $rapport->enfant_id, ['class' => 'form-control'.($errors->has('enfant_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('enfant_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enfant_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    @if (auth()->user()->isAdmin())
                    <div class="form-group">
                        {!! Form::label('user_id', __('Assistant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('user_id', $users, $rapport->user_id, ['class' => 'form-control'.($errors->has('user_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('user_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('user_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    @endif
                    <div class="form-group">
                        {!! Form::label('text', __('Rapport'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::textarea('text', $rapport->text, ['class' => 'form-control'.($errors->has('text') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('text'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('text') !!}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection