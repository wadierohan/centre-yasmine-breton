@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Rapports') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('rapports.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    @component('components.exportrapports')@endcomponent
                    @component('components.filters.rapports', compact('orderBy', 'filterByUser', 'filterByEnfant', 'perPage'))@endcomponent
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Mois/Année') }}</th>
                                <th>{{ __('Enfant') }}</th>
                                <th>{{ __('Assistant') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rapports as $rapport)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($rapport->date)->format('m/Y') }}</td>
                                <td>{{ $rapport->enfant->fullname }}</td>
                                <td>{{ $rapport->user->name }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$rapport->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$rapport->id}}">
                                            <a class="dropdown-item" href="{{route('rapports.show', $rapport->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('rapports.edit', $rapport->id)}}">{{__('Modifier')}}</a>
                                            <a class="dropdown-item" href="{{route('rapports.duplicate', $rapport->id)}}">{{__('Dupliquer')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'rapports.destroy', $rapport->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection