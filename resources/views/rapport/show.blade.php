@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Rapport') }} - {{ \Carbon\Carbon::parse($rapport->date)->format('m/Y') }} - {{ $rapport->enfant->fullname }} - {{ $rapport->user->name }} ({{ $rapport->user->service->name }})</div>

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectMonth('month', \Carbon\Carbon::parse($rapport->date)->month, ['class' => 'mymonthpicker form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::parse($rapport->date)->year, ['class' => 'myyearpicker form-control', 'disabled' => 'disabled']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('enfant_id', __('Enfant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfant_id', $enfants, $rapport->enfant_id, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                    </div>
                    @if (auth()->user()->isAdmin())
                    <div class="form-group">
                        {!! Form::label('user_id', __('Assistant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('user_id', $users, $rapport->user_id, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                    </div>
                    @endif
                    <div class="form-group">
                        {!! Form::label('text', __('Rapport'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::textarea('text', $rapport->text, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection