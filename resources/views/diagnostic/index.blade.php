@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Diagnostics') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('diagnostics.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Enfants') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Diagnostic::all() as $diagnostic)
                            <tr>
                                <td>{{ $diagnostic->name }}</td>
                                <td>{{ $diagnostic->enfants->count() }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$diagnostic->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$diagnostic->id}}">
                                            <a class="dropdown-item" href="{{route('diagnostics.show', $diagnostic->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('diagnostics.edit', $diagnostic->id)}}">{{__('Modifier')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'diagnostics.destroy', $diagnostic->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection