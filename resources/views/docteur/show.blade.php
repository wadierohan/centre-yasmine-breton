@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $docteur->name }}</div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', $docteur->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', __('Email'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::email('email', $docteur->email, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', __('Téléphone'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('phone', $docteur->phone, ['class' => 'form-control'.($errors->has('phone') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('phone') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('diagnostic_id', __('Diagnostic'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('diagnostic_id', App\Diagnostic::all(['id', 'name'])->pluck('name', 'id'), $docteur->diagnostic, ['class' => 'form-control'.($errors->has('diagnostic_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('diagnostic_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('diagnostic_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
