@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Docteurs') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('docteurs.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Téléphone') }}</th>
                                <th>{{ __('Diagnostic') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Docteur::all() as $docteur)
                            <tr>
                                <td>{{ $docteur->name }}</td>
                                <td>{{ $docteur->email }}</td>
                                <td>{{ $docteur->phone }}</td>
                                <td>{{ $docteur->diagnostic ? $docteur->diagnostic->name : '' }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$docteur->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$docteur->id}}">
                                            <a class="dropdown-item" href="{{route('docteurs.show', $docteur->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('docteurs.edit', $docteur->id)}}">{{__('Modifier')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'docteurs.destroy', $docteur->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection