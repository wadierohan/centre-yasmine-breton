@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Rendez-vous de') }} {{ $enfant->fullname }}</div>

                <div class="card-body">
                    <div id="calendar"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            locale: 'fr',
            initialView: 'dayGridMonth',
            eventTimeFormat: {
                hour: 'numeric',
                minute: '2-digit',
                meridiem: false
            },
            eventClick: function(info) {
                const fullDateStart = new Date(info.event.start)
                const fullDateEnd = new Date(info.event.end)
                const date = ('0' + fullDateStart.getDate()).slice(-2) + '/' + ('0' + fullDateStart.getMonth()).slice(-2) + '/' + fullDateStart.getFullYear()
                const start = ('0' + fullDateStart.getHours()).slice(-2) + ':' + ('0' + fullDateStart.getMinutes()).slice(-2)
                const end = ('0' + fullDateEnd.getHours()).slice(-2) + ':' + ('0' + fullDateEnd.getMinutes()).slice(-2)
                swal({   
                    title: "Plus d'info",
                    text: date + ' de ' + start + ' à ' + end + ' : ' + info.event.title
                })
            },
            events: <?= json_encode($events) ?>
        });
        calendar.render();
    })
</script>
@endsection