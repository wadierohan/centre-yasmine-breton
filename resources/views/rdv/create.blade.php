@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Nouveau rendez-vous') }}</div>

                <div class="card-body">
                    {{ Form::open([ 'method'  => 'POST', 'route' => [ 'rdvs.store' ], 'autocomplete' => 'off', 'files' => true ]) }}
                    <div class="form-group">
                        {!! Form::label('enfant_id', __('Enfant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfant_id', $enfants, Null, ['class' => 'form-control'.($errors->has('enfant_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('enfant_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enfant_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('user_id', __('Assistant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('user_id', $users, Null, ['class' => 'form-control'.($errors->has('user_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('user_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('user_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            {!! Form::label('date', __('Date'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('date', null, ['class' => 'datepicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('date') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('time_start', __('Heure début'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('time_start', $hours, Null, ['class' => 'form-control'.($errors->has('time_start') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('time_start'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('time_start') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('time_end', __('Heure début'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::select('time_end', $hours, Null, ['class' => 'form-control'.($errors->has('time_end') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('time_end'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('time_end') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection