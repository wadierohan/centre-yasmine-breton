@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Rendez-vous') }} {{ $rdv->enfant->fullname }} - {{ $rdv->user->name }} - {{ \Carbon\Carbon::parse($rdv->dateDebut)->format('d/m/Y H:i') }} - {{ \Carbon\Carbon::parse($rdv->dateFin)->format('d/m/Y H:i') }}</div>

                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('enfant_id', __('Enfant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('enfant_id', $rdv->enfant->fullname, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('user_id', __('Assistant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('user_id', $users[$rdv->user_id], ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            {!! Form::label('date', __('Date'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('date', \Carbon\Carbon::parse($rdv->dateDebut)->format('Y-m-d'), ['class' => 'datepicker form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('time_start', __('Heure début'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('time_start', \Carbon\Carbon::parse($rdv->dateDebut)->format('H:i'), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            {!! Form::label('time_end', __('Heure début'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('time_end', \Carbon\Carbon::parse($rdv->dateFin)->format('H:i'), ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection