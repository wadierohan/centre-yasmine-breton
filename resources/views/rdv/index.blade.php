@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Rendez-vous') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('rdvs.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Enfant') }}</th>
                                <th>{{ __('Assistant') }}</th>
                                <th>{{ __('Date de début') }}</th>
                                <th>{{ __('Date de fin') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rdvs as $rdv)
                            <tr>
                                <td>{{ $rdv->enfant->fullname }}</td>
                                <td>{{ $rdv->user->name }} ({{ $rdv->user->service->name }})</td>
                                <td>{{ \Carbon\Carbon::parse($rdv->dateDebut)->format('d/m/Y H:i') }}</td>
                                <td>{{ \Carbon\Carbon::parse($rdv->dateFin)->format('d/m/Y H:i') }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$rdv->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$rdv->id}}">
                                            <a class="dropdown-item" href="{{route('rdvs.show', $rdv->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('rdvs.edit', $rdv->id)}}">{{__('Modifier')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'rdvs.destroy', $rdv->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection