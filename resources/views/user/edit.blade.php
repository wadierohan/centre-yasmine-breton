@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $user->name }}</div>

                <div class="card-body">
                    {{ Form::open([ 'method'  => 'patch', 'route' => [ 'users.update', $user->id ], 'autocomplete' => 'off', 'files' => true ]) }}
                    <div class="form-group">
                        {!! Form::label('role', __('Rôle'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('role', ['assistant' => __('Assistant'), 'admin' => __('Admin') ], $user->role, ['class' => 'form-control'.($errors->has('role') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('role'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('role') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', $user->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', __('Email'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::email('email', $user->email, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', __('Téléphone'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('phone', $user->phone, ['class' => 'form-control'.($errors->has('phone') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('phone') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', __('Mot de passe'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::password('password', ['class' => 'form-control'.($errors->has('password') ? ' is-invalid' : ''), 'autocomplete' => 'new-password']) !!}
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('password') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('service_id', __('Service'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('service_id', App\Service::all(['id', 'name'])->pluck('name', 'id'), $user->service_id, ['class' => 'form-control'.($errors->has('service_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('service_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('service_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection