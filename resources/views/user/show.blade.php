@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $user->name }}</div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('role', __('Rôle'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('role', $user->role, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', $user->name, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', __('Email'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::email('email', $user->email, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', __('Téléphone'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('phone', $user->phone, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('service_id', __('Service'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('service_id', $user->service ? $user->service->name : null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection