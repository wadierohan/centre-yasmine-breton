@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Utilisateurs') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('users.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Role') }}</th>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Téléphone') }}</th>
                                <th>{{ __('Service') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\User::all() as $user)
                            <tr>
                                <td>{{ ucfirst($user->role) }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->service ? $user->service->name : '' }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$user->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$user->id}}">
                                            <a class="dropdown-item" href="{{route('users.show', $user->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('users.edit', $user->id)}}">{{__('Modifier')}}</a>
                                            @if($user->rdvs->count() > 0)
                                            <a class="dropdown-item" href="{{route('rdvs.showUserRdv', $user->id)}}">{{__('Voir les rendez-vous')}}</a>
                                            <div class="dropdown-divider"></div>
                                            @endif
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'users.destroy', $user->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection