<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }}</title>

    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
        }
        .page-break {
            page-break-after: always;
        }
        .page{
            position: relative;
            display: block;
            overflow: hidden;
            height: 27cm;
        }
        footer{
            bottom: 0;
            position: absolute;
            width: 600px;
            left: 0;
            right: 0;
            margin: 0 auto;
        }
        @media print {
            .page{
                /*width: 21cm;*/
                height: 27cm;
                /*margin: 30mm 45mm 30mm 45mm;*/
            }
        }

        header{
            padding-bottom: 20px;
        }
        .float-left{
            float:left;
        }
        .float-right{
            float:right;
        }
        .centre-logo-span img{
            width: 150px;
        }
        .clear-both{
            clear: both;
        }
        table{
            width: 100%;
            margin: 10px 0px;
        }
        table, td, th{
            border: 1px solid black;
            border-collapse: collapse;
        }
        td, th{
            padding: 15px;
        }
        th{
            text-align: center;
            background-color: #ccc;
        }
    </style>
</head>
<body>
    @foreach($prises as $prise)
    <div class="page">
        <header>
            <span id="centre-address-span" class="float-left">
                Centre Yasmine Breton<br>
                2, avenue Lalla Amina, Tabriquet, Salé<br>
                0537 85 64 70 / 0661 34 08 95<br>
                contact@centreyasmine.com
            </span>
            <span class="float-right centre-logo-span">
                <img src="{{ asset('img/logo.jpg') }}">
            </span>
            <div class="clear-both"></div>
        </header>

        <div id="body">
            <p class="float-right"><strong>Date : {{ $prise->facture_date ? \Carbon\Carbon::parse($prise->facture_date)->format('d/m/Y') : \Carbon\Carbon::now()->format('d/m/Y') }}</strong></p>
            <div class="clear-both"></div>
            <p><strong>Pour : {{ $prise->assurance->fullname }}</strong></p>
            <br>
            <p><strong>Enfant : {{ $prise->enfant->fullname }}</strong></p>
            <p><strong>Mois : {{ Carbon\Carbon::parse($prise->date)->endOfMonth()->isWeekend() ? Carbon\Carbon::parse($prise->date)->endOfMonth()->subDay()->format('d/m/Y') : Carbon\Carbon::parse($prise->date)->endOfMonth()->format('d/m/Y') }}</strong></p>
            <p><strong>Numéro prise en charge : {{ $prise->code }}</strong></p>
            <br>
            <p><center><strong>Facture N° {{$prise->facture}} / {{$prise->facture_year ?? Carbon\Carbon::parse($prise->date)->format('Y')}}</strong></center></p>
            <br><br>
            <table>
                <thead>
                    <tr>
                        <th>Désignation</th>
                        <th>Prix</th>
                    </tr>
                </thead>
                <tbody>
                    @if($prise->assurance->id === 1)
                    <tr>
                        <td>Suivi pluridisciplinaire pour les adhérents des FAR</td>
                        <td>1000.00 Dhs</td>
                    </tr>
                    @else
					@foreach($prise->enfant->users as $key => $user)
                    <tr>
                        <td>{{$user->service->name}}</td>
                        <td>{{($prise->service_id === 0 && $key === 0 || $prise->service_id === $user->service->id) ? money_format("%i Dhs", $prise->charge) : "0.00 Dhs"}}</td>
                    </tr>
					@endforeach
                    @endif
                </tbody>
            </table>
            <cite>Arrêté cette facture au montant de {{ $prise->wordCharge }} dirhams{{$prise->service && $prise->service->tva ? ', dont TVA 20%' : ''}}</cite>
            <br><br><br>
            <p>
                Rib domicilié à la banque populaire:
                <br><strong>181 815 212 11 03 49 85 7000 3 03</strong>
            </p>
        </div>

        <footer>
            <p><center>IF : 20789139 / ICE : 001857936000054 / Patent : 29098019 / CNSS : 5518769 /</center></p>
        </footer>
    </div>
    <div class="page-break"></div>
    @endforeach
</body>
</html>
