@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Tableau de bord</h1>
    <div class="row justify-content-center">
        <div class="col-md-3 d-flex align-items-stretch">
            <div class="card bg-warning w-100">
                <div class="card-body">
                    <h5 class="card-title">Nombre d'enfants</h5>
                    <h2 class="card-text">{{ $nbEnfants }}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3 d-flex align-items-stretch">
            <div class="card text-white bg-success w-100">
                <div class="card-body">
                    <h5 class="card-title">Nombre de prise en charge ce mois</h5>
                    <h2 class="card-text">{{ $nbPrises }}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3 d-flex align-items-stretch">
            <div class="card text-white bg-danger w-100">
                <div class="card-body">
                    <h5 class="card-title">Nombre de rapports ce mois</h5>
                    <h2 class="card-text">{{ $nbRapports }}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3 d-flex align-items-stretch">
            <div class="card text-white bg-info w-100">
                <div class="card-body">
                    <h5 class="card-title">Nombre de Rdv futurs</h5>
                    <h2 class="card-text">{{ $nbRdvs }}</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
