@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Nouveau tuteur') }}</div>

                <div class="card-body">
                    {{ Form::open([ 'method'  => 'POST', 'route' => [ 'tuteurs.store' ], 'autocomplete' => 'off', 'files' => true ]) }}
                    <div class="form-group">
                        {!! Form::label('firstname', __('Prénom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('firstname', null, ['class' => 'form-control'.($errors->has('firstname') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('firstname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('firstname') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('lastname', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('lastname', null, ['class' => 'form-control'.($errors->has('lastname') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('lastname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('lastname') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('type', __('Type'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('type', ['Père' => __('Père'), 'Mère' => __('Mère'), 'Autre' => __('Autre')], Null, ['class' => 'form-control'.($errors->has('type') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('type'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('type') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', __('Email'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::email('email', null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', __('Phone'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('phone', null, ['class' => 'form-control'.($errors->has('phone') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('phone') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('enfants', __('Enfants'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfants[]', App\Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id'), Null, ['class' => 'tags form-control'.($errors->has('enfants') ? ' is-invalid' : ''), 'multiple' => 'multiple']) !!}
                        @if ($errors->has('enfants'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enfants') !!}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection