@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Tuteurs') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('tuteurs.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Prénom') }}</th>
                                <th>{{ __('Type') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Phone') }}</th>
                                <th>{{ __('Enfants') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Tuteur::all() as $tuteur)
                            <tr>
                                <td>{{ $tuteur->lastname }}</td>
                                <td>{{ $tuteur->firstname }}</td>
                                <td>{{ __($tuteur->type) }}</td>
                                <td>{{ $tuteur->email }}</td>
                                <td>{{ $tuteur->phone }}</td>
                                <td>
                                    @foreach($tuteur->enfants as $enfant)
                                    <span class="badge badge-light">{{ $enfant->fullname }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$tuteur->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$tuteur->id}}">
                                            <a class="dropdown-item" href="{{route('tuteurs.show', $tuteur->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('tuteurs.edit', $tuteur->id)}}">{{__('Modifier')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'tuteurs.destroy', $tuteur->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection