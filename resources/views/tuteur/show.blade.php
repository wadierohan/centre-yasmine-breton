@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $tuteur->fullname }}</div>

                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('firstname', __('Prénom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('firstname', $tuteur->firstname, ['class' => 'form-control'.($errors->has('firstname') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('firstname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('firstname') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('lastname', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('lastname', $tuteur->lastname, ['class' => 'form-control'.($errors->has('lastname') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('lastname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('lastname') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('type', __('Type'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('type', ['Père' => __('Père'), 'Mère' => __('Mère'), 'Autre' => __('Autre')], $tuteur->type, ['class' => 'form-control'.($errors->has('type') ? ' is-invalid' : ''), 'disabled' => 'disabled']) !!}
                        @if ($errors->has('type'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('type') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', __('Email'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::email('email', $tuteur->email, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', __('Phone'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('phone', $tuteur->phone, ['class' => 'form-control'.($errors->has('phone') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('phone') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('enfants', __('Enfants'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfants[]', App\Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id'), $tuteur->enfants, ['class' => 'tags form-control'.($errors->has('enfants') ? ' is-invalid' : ''), 'multiple' => 'multiple', 'disabled' => 'disabled']) !!}
                        @if ($errors->has('enfants'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enfants') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
