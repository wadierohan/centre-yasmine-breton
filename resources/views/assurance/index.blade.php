@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Assurances') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('assurances.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Nom complet') }}</th>
                                <th>{{ __('Nom racourci') }}</th>
                                <th>{{ __('Nombre d\'enfants') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Assurance::all() as $assurance)
                            <tr>
                                <td>{{ $assurance->name }}</td>
                                <td>{{ $assurance->fullname }}</td>
                                <td>{{ $assurance->shortname }}</td>
                                <td>{{ $assurance->enfants()->count() }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$assurance->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$assurance->id}}">
                                            <a class="dropdown-item" href="{{route('assurances.show', $assurance->id)}}">{{__('Voir')}}</a>
                                            @if($assurance->id > 1)
                                            <a class="dropdown-item" href="{{route('assurances.edit', $assurance->id)}}">{{__('Modifier')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'assurances.destroy', $assurance->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection