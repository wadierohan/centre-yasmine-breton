@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $assurance->name }}</div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', $assurance->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('fullname', __('Nom complet'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('fullname', $assurance->fullname, ['class' => 'form-control'.($errors->has('fullname') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('fullname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('fullname') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('shortname', __('Nom racourci'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('shortname', $assurance->shortname, ['class' => 'form-control'.($errors->has('shortname') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('shortname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('shortname') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection