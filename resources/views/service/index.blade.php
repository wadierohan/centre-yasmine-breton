@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Services') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('services.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('TVA') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Service::all() as $service)
                            <tr>
                                <td>{{ $service->name }}</td>
                                <td>{{ $service->tva ? __('Oui') : __('Non') }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$service->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$service->id}}">
                                            <a class="dropdown-item" href="{{route('services.show', $service->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('services.edit', $service->id)}}">{{__('Modifier')}}</a>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'services.destroy', $service->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection