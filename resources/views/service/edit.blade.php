@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $service->name }}</div>

                <div class="card-body">
                    {{ Form::open([ 'method'  => 'patch', 'route' => [ 'services.update', $service->id ], 'autocomplete' => 'off', 'files' => true ]) }}
                    <div class="form-group">
                        {!! Form::label('name', __('Nom'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('name', $service->name, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('tva', __('TVA'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::checkbox('tva', 1, $service->tva) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('desc', __('Description'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::textarea('desc', $service->desc, ['class' => 'form-control'.($errors->has('desc') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('desc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('desc') !!}</strong>
                            </span>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection