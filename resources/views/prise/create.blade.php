@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Nouvelle prise en charge') }}</div>

                <div class="card-body">
                    {{ Form::open([ 'method'  => 'POST', 'route' => [ 'prises.store' ], 'autocomplete' => 'off', 'files' => true ]) }}
                    <div class="form-group">
                        {!! Form::label('code', __('Code'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('code', null, ['class' => 'form-control'.($errors->has('code') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('code'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('code') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            {!! Form::label('facture', __('Facture'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('facture', null, ['class' => 'form-control'.($errors->has('facture') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('facture'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('facture') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-2">
                            {!! Form::label('separator', '.', ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('separator', '/', ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="form-group col-md-5">
                            {!! Form::label('facture_year', __('Année facture'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::number('facture_year', \Carbon\Carbon::now()->year, ['class' => 'form-control'.($errors->has('facture_year') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('facture_year'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('facture_year') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('charge', __('Charge'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('charge', 1000, ['class' => 'form-control'.($errors->has('charge') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('charge'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('charge') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('service_id', __('Service en charge'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('service_id', App\Service::all(['id', 'name'])->pluck('name', 'id'), Null, ['class' => 'form-control'.($errors->has('service_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('service_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('service_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectMonth('month', \Carbon\Carbon::now()->month, ['class' => 'mymonthpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                            @if ($errors->has('date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('date') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::now()->year, ['class' => 'myyearpicker form-control'.($errors->has('date') ? ' is-invalid' : '')]) !!}
                        </div>
                        {!! Form::hidden('date', \Carbon\Carbon::now()->format('Y-m-01'), ['class' => 'mydate']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('enfant_id', __('Enfant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfant_id', App\Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id'), Null, ['class' => 'form-control'.($errors->has('enfant_id') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('enfant_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enfant_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('facture_date', __('Date de facture'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::date('facture_date', Null, ['class' => 'form-control'.($errors->has('facture_date') ? ' is-invalid' : '')]) !!}
                        @if ($errors->has('facture_date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('facture_date') !!}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::submit(__('Valider'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
