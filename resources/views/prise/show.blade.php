@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Prise en charge') }} - {{ \Carbon\Carbon::parse($prise->date)->format('m/Y') }} - {{ $prise->enfant->fullname }}</div>

                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('code', __('Code'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('code', $prise->code, ['class' => 'form-control'.($errors->has('code') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('code'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('code') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            {!! Form::label('facture', __('Facture'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('facture', $prise->facture, ['class' => 'form-control'.($errors->has('facture') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                            @if ($errors->has('facture'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('facture') !!}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-2">
                            {!! Form::label('separator', '.', ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::text('separator', '/', ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        </div>
                        <div class="form-group col-md-5">
                            {!! Form::label('facture_year', __('Année facture'), ['class' => 'col-form-label text-md-right']) !!}
                            {!! Form::number('facture_year', $prise->facture_year ?? \Carbon\Carbon::parse($prise->date)->year, ['class' => 'form-control'.($errors->has('facture_year') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                            @if ($errors->has('facture_year'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('facture_year') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('charge', __('Charge'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::text('charge', $prise->charge, ['class' => 'form-control'.($errors->has('charge') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('charge'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('charge') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('service_id', __('Service en charge'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('service_id', App\Service::all(['id', 'name'])->pluck('name', 'id'), $prise->service_id, ['class' => 'form-control'.($errors->has('service_id') ? ' is-invalid' : ''), 'readonly' => 'readonly']) !!}
                        @if ($errors->has('service_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('service_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {!! Form::label('month', __('Mois'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectMonth('month', \Carbon\Carbon::parse($prise->date)->month, ['class' => 'mymonthpicker form-control'.($errors->has('date') ? ' is-invalid' : ''), 'disabled' => 'disabled']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('year', __('Année'), ['class' => 'col-form-label']) !!}
                            {!! Form::selectYear('year', config('app.year'), \Carbon\Carbon::now()->addYears(5)->year, \Carbon\Carbon::parse($prise->date)->year, ['class' => 'myyearpicker form-control'.($errors->has('date') ? ' is-invalid' : ''), 'disabled' => 'disabled']) !!}
                        </div>
                        {!! Form::hidden('date', $prise->date, ['class' => 'mydate']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('enfant_id', __('Enfant'), ['class' => 'col-form-label text-md-right']) !!}
                        {!! Form::select('enfant_id', App\Enfant::all(['id', 'firstname', 'lastname'])->pluck('fullname', 'id'), $prise->enfant_id, ['class' => 'form-control'.($errors->has('enfants') ? ' is-invalid' : ''), 'disabled' => 'disabled']) !!}
                        @if ($errors->has('enfant_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{!! $errors->first('enfant_id') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
