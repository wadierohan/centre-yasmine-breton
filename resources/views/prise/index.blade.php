@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Prise en charges') }}
                    <a class="btn btn-primary float-sm-right" href="{{route('prises.create')}}" role="button">{{__('Ajouter')}}</a>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ __('Nom') }}</th>
                                <th>{{ __('Prénom') }}</th>
                                <th>{{ __('Code') }}</th>
                                <th>{{ __('Facture') }}</th>
                                <th>{{ __('Année facture') }}</th>
                                <th>{{ __('Charge') }}</th>
                                <th>{{ __('Service en charge') }}</th>
                                <th>{{ __('Date') }}</th>
                                <th>{{ __('Date facture') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($prises as $prise)
                            <tr id="{{ $prise->id }}">
                                <td scope="row">{{ $prise->enfant->lastname }}</td>
                                <td>{{ $prise->enfant->firstname }}</td>
                                <td>{{ $prise->code }}</td>
                                <td>{{ $prise->facture }}</td>
                                <td>{{ $prise->facture_year ?? \Carbon\Carbon::parse($prise->date)->format('Y') }}</td>
                                <td>{{ $prise->charge }}</td>
                                <td>{{ $prise->service ? $prise->service->name : '' }}</td>
                                <td>{{ \Carbon\Carbon::parse($prise->date)->format('m/Y') }}</td>
                                <td>{{ $prise->facture_date ? \Carbon\Carbon::parse($prise->facture_date)->format('d/m/Y') : '' }}</td>
                                <td>
                                    <div class="dropdown float-right">
                                        <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$prise->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ __('Actions') }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$prise->id}}">
                                            <a class="dropdown-item" href="{{route('prises.show', $prise->id)}}">{{__('Voir')}}</a>
                                            <a class="dropdown-item" href="{{route('prises.edit', $prise->id)}}">{{__('Modifier')}}</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" target="_blank" href="{{route('prises.generatePdf', $prise->id)}}">{{__('Imprimer')}}</a>
                                            <div class="dropdown-divider"></div>
                                            {{ Form::open([ 'method'  => 'delete', 'route' => [ 'prises.destroy', $prise->id ] ]) }}
                                            <a class="dropdown-item delete-model" href="javascript:void(0)">{{__('Supprimer')}}</a>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
