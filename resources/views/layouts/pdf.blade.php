<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <div id="app">
        <header>
            <h1>Header</h1>
        </header>

        <main class="py-4">
            @yield('content')
        </main>

        <footer>
            <h3>Footer</h3>
        </footer>
    </div>
</body>
</html>
