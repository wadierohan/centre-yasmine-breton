<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }}</title>

    <style>
        body{
            font-family: Arial, Helvetica, sans-serif;
        }
        .page-break {
            page-break-after: always;
        }
        .page{
            position: relative;
            display: block;
            overflow: hidden;
            /* height: 27cm; */
        }
        footer{
            margin: 50px auto;
            border-top: 1px solid gray;
        }
        footer > center {
            margin-top: 6px;
        }
        @media print {
            .page{
                /*width: 21cm;*/
                /* height: 27cm; */
                /*margin: 30mm 45mm 30mm 45mm;*/
            }
        }

        header{
            padding-bottom: 20px;
            font-size: 12px;
            border-bottom: 1px solid gray;
        }
        .flex {
            display: flex;
        }
        .row {
            margin-bottom: 10px;
        }
        .text {
            padding: 10px;
            border: 1px solid gray;
        }
        .text.commentaire {
            height: 50px;
        }
        .float-left{
            float:left;
        }
        .float-right{
            float:right;
        }
        .centre-logo-span {
            margin-right: 50px;
        }
        .justify-between {
            justify-content: space-between;
        }
        .centre-logo-span img{
            width: 150px;
        }
        .clear-both{
            clear: both;
        }
        table{
            width: 100%;
            margin: 10px 0px;
        }
        table, td, th{
            border: 1px solid black;
            border-collapse: collapse;
        }
        td, th{
            padding: 15px;
        }
        th{
            text-align: center;
            background-color: #ccc;
        }
    </style>
</head>
<body>
    @foreach($data as $row)
    <div class="page">
        <header class="flex">
            <div class="centre-logo-span">
                <img src="{{ asset('img/logo.jpg') }}">
            </div>
            <div style="width: 100%;">
                <div class="flex justify-between row">
                    <span>Enfant: {{ $row['enfant']->fullname }}</span>
                    <span>Date de naissance: {{ \Carbon\Carbon::parse($row['enfant']->birthdate)->format('d/m/Y') }}</span>
                    <span>Rapport du: {{ \Carbon\Carbon::parse($row['date'])->format('m/Y') }}</span>
                </div>
                <div class="flex justify-between row">
                    <span>Diagnostic: {{ $row['enfant']->diagnostic->name }}</span>
                    <span>N° de prise en charge: {{ $row['prise']->code ?? 'Pas encore attribué' }}</span>
                </div>
                <div class="flex justify-between row">
                    <span>Suivi en: {{ $row['services'] }}</span>
                </div>
            </div>
            <div class="clear-both"></div>
        </header>

        <div id="body">
            <center><h1>Rapport de suivi</h1></center>
            <h3>Anamnèse :</h3>
            <div class="text">{{ $row['enfant']->anamnese }}</div>
            @foreach($row['rapports'] as $rapport)
            <h3>{{ $rapport->user->service->name }} :</h3>
            <div class="text">{{ $rapport->text }}</div>
            @endforeach
        </div>

        <footer>
            <center>IF : 20789139 / ICE : 001857936000054 / Patent : 29098019 / CNSS : 5518769 /</center>
            <center>www.centreyasmine.com | centreyasminebreton@gmail.com | +212 661 34 08 95</center>
            <center>2, avenue lalla amina, Tabriquet - Salé</center>
            <center>28, rue tonkin, App 1, diour jamaa - Rabat</center>
        </footer>
    </div>
    <div class="page-break"></div>
    @endforeach
</body>
</html>
